import java.util.Scanner;

class Naloga2{
	static int count;
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		if(args[1].equals("os")){
			String a=input.nextLine(); String b=input.nextLine();
			int t[][]=new int[2][10000];
			int n1=a.length(), n2=b.length(), n3=0, m1=0, m2=0, tmpC, c, d, e, f, g, i, j, k, l;
			while(a.charAt(m1)=='0') m1++;
			while(b.charAt(m2)=='0') m2++;
			
			if(args[0].equals("count")){
				g=m2;
				for(i=m2; i<n2; i++)
					if(b.charAt(i)=='0') g++;
				System.out.println((n1-m1)*(n2-g));
			}else{
				e=1; f=1;
				for(i=m2; i<n2; i++){
					l=1; k=0;
					for(j=n1-1; j>=m1; j--){
						if(b.charAt(i)=='0'){ System.out.println("0"); l=0; f++; break; }
						t[0][k++]=Character.getNumericValue(a.charAt(j))*Character.getNumericValue(b.charAt(i));
					}
					if(l==0) continue;
					
					d=0; j=k;
					while(d!=10){
						if(t[0][j]!=0){ t[0][j]=0; d=0; }
						else d++;
						j++;
					}
					
					c=0;
					for(j=0; j<k; j++)
						if(t[0][j]>9){
							tmpC=t[0][j]/10;
							t[0][j]%=10;
							t[0][j+1]+=tmpC;
							if(j==k-1) c=1;
						}
					if(c==1) k++;
					
					for(j=k-1; j>=0; j--)
						System.out.print(t[0][j]);
					System.out.println();
					
					if(e==1){
						for(j=0; j<k; j++){
							t[1][j]=t[0][j];
						}
						e=0; n3=k;
					}else{
						for(j=0; j<n3; j++)
							t[0][j+f]+=t[1][j];
						n3+=f;
						f=1;
						if(n3>k) k=n3;
						c=0;
						for(j=0; j<k; j++)
							if(t[0][j]>9){
								t[0][j]-=10; t[0][j+1]++;
								if(j==k-1) c=1;
							}
						if(c==1) k++;
					}
					
					for(j=0; j<k; j++)
						t[1][j]=t[0][j];
					n3=k;
					
					d=0; j=k;
					while(d!=20){
						if(t[0][j]!=0){ t[0][j]=0; d=0; }
						else d++;
						j++;
					}
				}
				
				for(j=n3-1; j>=0; j--)
					System.out.print("-");
				if(f!=1)
					for(j=0; j<f-1; j++)
						System.out.print("-");
				System.out.println();
				for(j=n3-1; j>=0; j--)
					System.out.print(t[1][j]);
				if(f!=1)
					for(j=0; j<f-1; j++)
						System.out.print("0");
				System.out.println();
			}
		}else if(args[1].equals("ru")){
			int t[][]=new int[3][10000], t2[][]=new int[2][10000], c, tmpC;
			String a=input.nextLine(); String b=input.nextLine(), neparno;
			int n1=a.length(), n2=b.length(), n3=-1;
			int i, j, sumOfDigits=0, index=1, end=0, zeros=1, sumZeros=-1, prazno=1;
			
			for(j=0; j<n1; j++){
				t[0][j]=Character.getNumericValue(a.charAt(j));
				if(args[0].equals("trace")) System.out.print(t[0][j]);
			}
			if(args[0].equals("trace")) System.out.print(" ");
			neparno=t[0][n1-1]%2==0 ? "0" : "1";
			i=0;
			if(neparno.charAt(0)=='1'){
				for(j=n2-1; j>=0; j--){
					t2[0][i]=Character.getNumericValue(b.charAt(j));
					t[2][i]=t2[0][i];
					i++;
				}
				
				prazno=0; n3=n2;
			}else{
				for(j=n2-1; j>=0; j--)
					t2[0][i++]=Character.getNumericValue(b.charAt(j));
			}
			if(args[0].equals("trace")){
				for(j=n2-1; j>=0; j--)
					System.out.print(t2[0][j]);
				System.out.println(" "+neparno);
			}
			while(end!=1){
				sumZeros=0;
				for(j=0; j<n1; j++){
					if(t[index%2][j]==0) sumZeros++;
					else break;
				}
				sumOfDigits+=n1-sumZeros+n2;
				
				for(j=0; j<n1; j++){
					if(t[(index+1)%2][j]%2==1) t[(index+1)%2][j+1]+=10;
					t[index%2][j]=t[(index+1)%2][j]/2;
				}
				
				for(j=0; j<n2; j++)
					t2[index%2][j]=t2[(index+1)%2][j]*2;
				c=0;
				for(j=0; j<n2; j++)
					if(t2[index%2][j]>9){
						tmpC=t2[index%2][j]/10;
						t2[index%2][j]%=10; t2[index%2][j+1]+=tmpC;
						if(j==n2-1) c=1;
					}
				if(c==1) n2++;
				
				zeros=1; sumZeros=0;
				for(j=0; j<n1; j++){
					if(zeros==1 && t[index%2][j]==0){ sumZeros++; continue; }
					else zeros=0;
					if(args[0].equals("trace")) System.out.print(t[index%2][j]);
				}
				if(args[0].equals("trace")){
					System.out.print(" ");
					for(j=n2-1; j>=0; j--)
						System.out.print(t2[index%2][j]);
					neparno=t[index%2][n1-1]%2==0 ? "0" : "1";
					System.out.println(" "+neparno);
				}
				if(neparno.charAt(0)=='1'){
					if(prazno==1){
						for(j=0; j<n2; j++)
							t[2][j]=t2[index%2][j];
						prazno=0; n3=n2;
					}else{
						for(j=0; j<n2; j++)
							t[2][j]+=t2[index%2][j];
						if(n2>n3) n3=n2;
						c=0;
						for(j=0; j<n3; j++)
							if(t[2][j]>9){
								t[2][j]-=10; t[2][j+1]++;
								if(j==n3-1) c=1;
							}
						if(c==1) n3++;
					}
				}
				
				end=1;
				if(t[index%2][n1-1]!=1) end=0;
				if(end==1)
					for(j=0; j<n1-1; j++)
						if(t[index%2][j]!=0){ end=0; break; }
				
				index++;
			}
			if(args[0].equals("trace")){
				for(j=n3-1; j>=0; j--)
					System.out.print(t[2][j]);
				System.out.println();
			}
			if(args[0].equals("count")) System.out.println(sumOfDigits+1);
		}else if(args[1].equals("dv")){
			count=0;
			String a=input.nextLine(); String b=input.nextLine();
			int i, j, n1=a.length(), n2=b.length(), maxLength;
			maxLength=(n1>=n2 ? n1 : n2);
			if(maxLength%2==1) maxLength++;
			
			int t[][]=new int[2][maxLength];
			for(i=n1-1, j=0; i>=0; i--)
				t[0][j++]=Character.getNumericValue(a.charAt(i));
			for(i=n2-1, j=0; i>=0; i--)
				t[1][j++]=Character.getNumericValue(b.charAt(i));
			
			int t2[]=multiply(t[0], t[1], (args[0].equals("trace") ? 1 : 0));
			if(args[0].equals("trace")) print(t2);
			else System.out.println(count);
		}else if(args[1].equals("ka")){
			count=0;
			String a=input.nextLine(); String b=input.nextLine();
			int i, j, n1=a.length(), n2=b.length(), maxLength;
			maxLength=(n1>=n2 ? n1 : n2);
			if(maxLength%2==1) maxLength++;
			
			int t[][]=new int[2][maxLength];
			for(i=n1-1, j=0; i>=0; i--)
				t[0][j++]=Character.getNumericValue(a.charAt(i));
			for(i=n2-1, j=0; i>=0; i--)
				t[1][j++]=Character.getNumericValue(b.charAt(i));
			
			int t2[]=Karatsuba(t[0], t[1], (args[0].equals("trace") ? 1 : 0));
			if(args[0].equals("trace")) print(t2);
			else System.out.println(count);
		}
	}
	
	public static int[] multiply(int a[], int b[], int trace){
		if(trace==1){ print(a); System.out.print(" "); print(b); System.out.println(); }
		int tableLen=a.length>b.length ? a.length : b.length, zerosOfA=0, zerosOfB=0, countZerosForA=1, countZerosForB=1;
		
		for(int i=tableLen-1; i>=0; i--){
			if(countZerosForA==1 && a[i]==0) zerosOfA++;
			else countZerosForA=0;
			if(countZerosForB==1 && b[i]==0) zerosOfB++;
			else countZerosForB=0;
			if(countZerosForA==0 && countZerosForB==0) break;
		}
		
		int aLen=tableLen-zerosOfA, bLen=tableLen-zerosOfB;
		if(aLen==0 || bLen==0)
			return new int[1];
		if(aLen==1 || bLen==1){
			int i, tmp, t[]=new int[tableLen+1];
			if(aLen==1)
				for(i=0; i<bLen; i++){ t[i]=a[0]*b[i]; count++; }
			else if(bLen==1)
				for(i=0; i<aLen; i++){ t[i]=a[i]*b[0]; count++; }
			for(i=0; i<tableLen; i++)
				if(t[i]>9){ tmp=t[i]/10; t[i]%=10; t[i+1]+=tmp; }
			return t;
		}
		
		int lenOfNewTable=a.length+b.length;
		if(lenOfNewTable%2==1) lenOfNewTable++;
		int n=aLen>bLen ? aLen : bLen;
		if(n%2==1) n++;
		
		int n2=n/2;
		int aL[]=new int[n2], aR[]=new int[n2], bL[]=new int[n2], bR[]=new int[n2];
		
		for(int i=0, j=0; i<n; i++){
			if(i<n2){
				aR[i]=a[i];
				bR[i]=b[i];
			}else{
				if(i<a.length) aL[j]=a[i];
				if(i<b.length) bL[j]=b[i];
				j++;
			}
		}
		
		int m1[]=multiply(aR, bR, trace);
		int m2[]=multiply(aR, bL, trace);
		int m3[]=multiply(aL, bR, trace);
		int m4[]=multiply(aL, bL, trace);
		
		return add(m4, add(m3, m2), m1, n, lenOfNewTable);
	}
	
	public static int[] Karatsuba(int a[], int b[], int trace){
		if(trace==1){ print(a); System.out.print(" "); print(b); System.out.println(); }
		int tableLen=a.length>b.length ? a.length : b.length, zerosOfA=0, zerosOfB=0, countZerosForA=1, countZerosForB=1;
		
		for(int i=tableLen-1; i>=0; i--){
			if(countZerosForA==1 && a[i]==0) zerosOfA++;
			else countZerosForA=0;
			if(countZerosForB==1 && b[i]==0) zerosOfB++;
			else countZerosForB=0;
			if(countZerosForA==0 && countZerosForB==0) break;
		}
		
		int aLen=tableLen-zerosOfA, bLen=tableLen-zerosOfB;
		if(aLen==0 || bLen==0)
			return new int[1];
		if(aLen==1 || bLen==1){
			int i, tmp, t[]=new int[tableLen+1];
			if(aLen==1)
				for(i=0; i<bLen; i++){ t[i]=a[0]*b[i]; count++; }
			else if(bLen==1)
				for(i=0; i<aLen; i++){ t[i]=a[i]*b[0]; count++; }
			for(i=0; i<tableLen; i++)
				if(t[i]>9){ tmp=t[i]/10; t[i]%=10; t[i+1]+=tmp; }
			return t;
		}
		
		int lenOfNewTable=a.length+b.length;
		if(lenOfNewTable%2==1) lenOfNewTable++;
		int n=aLen>bLen ? aLen : bLen;
		if(n%2==1) n++;
		
		int n2=n/2;
		int aL[]=new int[n2], aR[]=new int[n2], bL[]=new int[n2], bR[]=new int[n2];
		
		for(int i=0, j=0; i<n; i++){
			if(i<n2){
				aR[i]=a[i];
				bR[i]=b[i];
			}else{
				if(i<a.length) aL[j]=a[i];
				if(i<b.length) bL[j]=b[i];
				j++;
			}
		}
		
		int m1[]=Karatsuba(aR, bR, trace);
		int m3[]=Karatsuba(aL, bL, trace);
		int m2[]=Karatsuba(add(aL, aR), add(bL, bR), trace);
		
		return add(m3, subtract(subtract(m2, m3), m1), m1, n, lenOfNewTable);
	}
	
	public static void print(int t[]){
		int zeros=1;
		for(int j=t.length-1; j>=0; j--){
			if(zeros==1 && t[j]==0) continue;
			else zeros=0;
			System.out.print(t[j]);
		}
		if(zeros==1) System.out.print("0");
	}
	
	public static int[] add(int a[], int b[]){
		int i, n=a.length>b.length ? a.length : b.length, t[]=new int[n+1];

		for(i=0; i<n; i++){
			if(i<a.length) t[i]+=a[i];
			if(i<b.length) t[i]+=b[i];
		}
		
		for(i=0; i<n; i++)
			if(t[i]>9){ t[i]-=10; t[i+1]++; }
		
		return t;
	}
	
	public static int[] add(int table1[], int table2[], int table3[], int offset, int lenOfNewTable){
		int i, t[]=new int[lenOfNewTable], halfOffset=offset/2;
		
		for(i=0; i<lenOfNewTable; i++){
			if(i<table1.length && table1[i]!=0) t[i+offset]+=table1[i];
			if(i<table2.length && table2[i]!=0) t[i+halfOffset]+=table2[i];
			if(i<table3.length && table3[i]!=0) t[i]+=table3[i];
		}
		
		for(i=0; i<lenOfNewTable-1; i++)
			if(t[i]>9){ t[i]-=10; t[i+1]++; }
		
		return t;
	}
	
	public static int[] subtract(int a[], int b[]){
		int i, n, m;
		if(a.length>b.length){ n=a.length; m=b.length; }
		else{ n=b.length; m=a.length; }
		int t[]=new int[n];

		for(i=0; i<n; i++){
			if(i<m){
				if(a[i]<b[i]){ a[i+1]--; a[i]+=10; }
				t[i]=a[i]-b[i];
			}else
				t[i]=a[i];
		}
		
		return t;
	}
}