import java.util.ArrayList;
import java.util.Stack;
import java.util.Scanner;

class Network{
	Node nodes[];
	
	/**
	 * Create a new network with n nodes (0..n-1).
	 * @param n the size of the network.
	 */
	public Network(int n){
		nodes = new Node[n];
		for(int i = 0; i < nodes.length; i++)
			nodes[i] = new Node(i);
	}
	/**
	 * Add a connection to the network, with all the corresponding in and out edges.
	 * @param fromNode
	 * @param toNode
	 */
	public void addConnection(int fromNode, int toNode){
		Edge e = new Edge(fromNode, toNode);
		nodes[fromNode].outEdges.add(e);
		nodes[toNode].inEdges.add(e);
		nodes[toNode].indegree++;
	}
}

class Node{
	int id, indegree;
	ArrayList<Edge> inEdges, outEdges;
	boolean visited;
	
	public Node(int i){
		id = i; indegree = 0;
		inEdges = new ArrayList<Edge>();
		outEdges = new ArrayList<Edge>();
		visited = false;
	}
}

class Edge{
	int startID, endID;
	
	public Edge(int fromNode, int toNode){
		startID = fromNode;
		endID = toNode;
	}
}

public class Naloga3{
	private static Network net;
	private static int result[], mode, sum;
	
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		int numOfNodes=input.nextInt();
		net=new Network(numOfNodes);
		
		while(input.hasNextInt())
			net.addConnection(input.nextInt(), input.nextInt());

		result=new int[numOfNodes];
		sum=0;
		if(args[0].equals("one"))
			printTopologicalSort(0);
		else if(args[0].equals("all")){
			mode=1;
			printAllTopologicalSorts(0);
		}else if(args[0].equals("count")){
			mode=2;
			printAllTopologicalSorts(0);
			System.out.printf("%d\n", sum);
		}
	}
	
	// recursive function to print all possible topological sorts
	private static void printAllTopologicalSorts(int j){
		// To indicate whether all topological are found or not
		boolean allVisited=true;
		
		for(int i=0; i<net.nodes.length; i++){
			// If indegree is 0 and not yet visited then only choose that vertex
			if(net.nodes[i].indegree==0 && !net.nodes[i].visited){
				// reducing indegree of adjacent vertices
				for(int e=0; e<net.nodes[i].outEdges.size(); e++)
					net.nodes[net.nodes[i].outEdges.get(e).endID].indegree--;
				
				// including in result
				result[j]=i;
				net.nodes[i].visited=true;
				
				printAllTopologicalSorts(j+1);
				
				// resetting visited and indegree for backtracking
				net.nodes[i].visited=false;
				for(int e=0; e<net.nodes[i].outEdges.size(); e++)
					net.nodes[net.nodes[i].outEdges.get(e).endID].indegree++;
				
				allVisited=false;
			}
		}
		
		// We reach here if all vertices are visited, we print the solution here
		if(allVisited){
			sum++;
			if(mode==1){
				for(int i=0; i<result.length; i++)
					System.out.printf("%d ", result[i]);
				System.out.printf("\n");
			}
		}
	}
	
	// recursive function to print the first sort in lexicographic order
	private static void printTopologicalSort(int j){
		boolean allVisited=true;
		for(int i=0; i<net.nodes.length; i++){
			if(net.nodes[i].indegree==0 && !net.nodes[i].visited){
				for(int e=0; e<net.nodes[i].outEdges.size(); e++)
					net.nodes[net.nodes[i].outEdges.get(e).endID].indegree--;
				net.nodes[i].visited=true;
				result[j]=i;
				printTopologicalSort(j+1);
				allVisited=false;
				break;
			}
		}
		if(allVisited){
			for(int i=0; i<result.length; i++)
				System.out.printf("%d ", result[i]);
			System.out.printf("\n");
		}
	}
}
