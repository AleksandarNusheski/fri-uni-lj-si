import java.util.Scanner;

class Naloga4{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int i, j, k, n = sc.nextInt(); sc.nextInt();
        int t[][]=new int[n][n], A[]=new int[n];
        
        for(i=0; i<n; i++){
			A[i]=i;
            for(j=0; j<n; j++)
                t[i][j]=sc.nextInt();
        }
		
		if(args[0].equals("opt"))
        	Backtrack(t, A, 0, 0, Integer.MAX_VALUE);
		else{
			int D[]=cna(n, t);
			otp2(n, D, t);	
		}
    }
	
	// cna = closest neighbours algorithm
	public static int[] cna(int n, int t[][]){
		int B[]=new int[n], C[]=new int[n], i, cIndex, sum, min, minIndex, current;
		cIndex=0;
        C[cIndex++]=0;
        B[0]=1;
        current=0;
        
        for(sum=n-1; sum!=0; sum--){
            min=Integer.MAX_VALUE; minIndex=-1;
            for(i=0; i<n; i++)
                if(B[i]==0 && (minIndex==-1 || t[current][i]<min)){
                    min=t[current][i];
                    minIndex=i;
                }
            B[minIndex]=1;
            C[cIndex++]=minIndex;
			current=minIndex;
        }
		return C;
	}
	
	public static void otp2(int n, int A[], int t[][]){
		int i, j, besti=-1, bestj=-1, pocetok=0;
		do{
			for(i=1; i<n-2; i++){
				for(j=i+1; j<n-1; j++){
					besti=-1; bestj=-1;
					if(t[A[i]][A[i-1]]+t[A[j+1]][A[j]]>t[A[i]][A[j+1]]+t[A[i-1]][A[j]]){
						if(besti==-1 && bestj==-1 || t[A[i]][A[i-1]]+t[A[j+1]][A[j]]>t[A[besti]][A[bestj+1]]+t[A[besti-1]][A[bestj]]){
							besti=i; bestj=j;
						}
					}
				}
				pocetok=0;
				if(besti!=-1 && bestj!=-1){
					A=swap2(A, besti, bestj);
					pocetok=1;
				}
				if(pocetok==1) break;
			}
			if(pocetok==1) continue;
		}while(false);
		
		int sum=0;
		for(i=1; i<n; i++)
            sum+=t[A[i-1]][A[i]];
		sum+=t[A[n-1]][A[0]];
		
		System.out.print(sum+": ");
		for(i=0; i<A.length; i++)
			System.out.print(A[i]+" ");
		System.out.println();
	}
	
	public static int[] swap2(int route[], int i, int k){
		int a, b, newRoute[]=new int[route.length];
		for(a=0; a<i; a++) newRoute[a]=route[a];
		for(b=k; b>=i; a++, b--) newRoute[a]=route[b];
		for(a=k+1; a<route.length; a++) newRoute[a]=route[a];
		return newRoute;
	}
	
	public static void swap(int i, int j, int[] arr){
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
    
    public static int Backtrack(int t[][], int A[], int l, int lengthSoFar, int minCost){
        int n=A.length;
        if(l==n-1){
        	int tmpMinCost=minCost;
            minCost=Math.min(minCost, lengthSoFar + t[A[n-1]][A[0]]);
            if(tmpMinCost!=minCost){
	            System.out.print(minCost+": ");
	            for(int j=0; j<n; j++)
	            	System.out.print(A[j]+" ");
	        	System.out.println();
            }
        }else
            for(int i=l+1; i<n; i++){
                swap(l+1, i, A);
                int newLength=lengthSoFar + t[A[l]][A[l+1]];
                if(newLength<minCost)
                    minCost = Math.min(minCost, Backtrack(t, A, l+1, newLength, minCost));
                swap(l+1, i, A);
            }
        return minCost;
    }
}
