import java.util.Scanner;

class Naloga1{
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		int t[], n, i;
		if(args.length>3){
			n=Integer.parseInt(args[3]);
			t=new int[n];
			for(i=0; i<n; i++)
				t[i]=input.nextInt();
		}else{
			n=16; i=0; t=new int[n];
			while(input.hasNextInt()){
				t[i++]=input.nextInt();
				if(i==n){
					int t2[]=new int[n*2];
					for(int j=0; j<n; j++)
						t2[j]=t[j];
					n*=2; t=t2;
				}
			}
		}
		input.close();
		
		int m=args.length>3?n:i;
		if(args[1].equals("bs")){
			if(args[0].equals("trace")){
				if(args[2].equals("up")) BubbleSort(t, m, 1, 1);
				else BubbleSort(t, m, 1, 0);
			}else{
				if(args[2].equals("up")){ BubbleSort(t, m, 0, 1); BubbleSort(t, m, 0, 1); BubbleSort(t, m, 0, 0); }
				else{ BubbleSort(t, m, 0, 0); BubbleSort(t, m, 0, 0); BubbleSort(t, m, 0, 1); }
			}
		}else if(args[1].equals("ss")){
			if(args[0].equals("trace")){
				if(args[2].equals("up")) SelectionSort(t, m, 1, 1);
				else SelectionSort(t, m, 1, 0);
			}else{
				if(args[2].equals("up")){ SelectionSort(t, m, 0, 1); SelectionSort(t, m, 0, 1); SelectionSort(t, m, 0, 0); }
				else{ SelectionSort(t, m, 0, 0); SelectionSort(t, m, 0, 0); SelectionSort(t, m, 0, 1); }
			}
		}else if(args[1].equals("is")){
			if(args[0].equals("trace")){
				if(args[2].equals("up")) InsertionSort(t, m, 1, 1);
				else InsertionSort(t, m, 1, 0);
			}else{
				if(args[2].equals("up")){ InsertionSort(t, m, 0, 1); InsertionSort(t, m, 0, 1); InsertionSort(t, m, 0, 0); }
				else{ InsertionSort(t, m, 0, 0); InsertionSort(t, m, 0, 0); InsertionSort(t, m, 0, 1); }
			}
		}else if(args[1].equals("hs")){
			if(args[0].equals("trace")){
				if(args[2].equals("up")) HeapSort(t, m-1, 1, 1);
				else HeapSort(t, m-1, 1, 0);
			}else{
				if(args[2].equals("up")){ HeapSort(t, m-1, 0, 1); HeapSort(t, m-1, 0, 1); HeapSort(t, m-1, 0, 0); }
				else{ HeapSort(t, m-1, 0, 0); HeapSort(t, m-1, 0, 0); HeapSort(t, m-1, 0, 1); }
			}
		}else if(args[1].equals("qs")){
			if(args[0].equals("trace")){
				if(args[2].equals("up")) QuickSort(t, 0, m-1, 1, 1);
				else QuickSort(t, 0, m-1, 1, 0);
			}else{
				if(args[2].equals("up")){
					int tp[]= QuickSort(t, 0, m-1, 0, 1); System.out.println(tp[0]+" "+tp[1]);
							tp=QuickSort(t, 0, m-1, 0, 1); System.out.println(tp[0]+" "+tp[1]);
							tp=QuickSort(t, 0, m-1, 0, 0); System.out.println(tp[0]+" "+tp[1]); }
				else{
					int tp[]= QuickSort(t, 0, m-1, 0, 0); System.out.println(tp[0]+" "+tp[1]);
							tp=QuickSort(t, 0, m-1, 0, 0); System.out.println(tp[0]+" "+tp[1]);
							tp=QuickSort(t, 0, m-1, 0, 1); System.out.println(tp[0]+" "+tp[1]); }
			}
		}else if(args[1].equals("ms")){
			if(args[0].equals("trace")){
				if(args[2].equals("up")){ MergeSort(t, new int[m], 0, m-1, 1, 1); printMS(t, 0, m-1, 0); }
				else{ MergeSort(t, new int[m], 0, m-1, 1, 0); printMS(t, 0, m-1, 0); }
			}else{
				if(args[2].equals("up")){
					int tp[]= MergeSort(t, new int[m], 0, m-1, 0, 1); System.out.println(tp[0]+" "+tp[1]);
							tp=MergeSort(t, new int[m], 0, m-1, 0, 1); System.out.println(tp[0]+" "+tp[1]);
							tp=MergeSort(t, new int[m], 0, m-1, 0, 0); System.out.println(tp[0]+" "+tp[1]); }
				else{
					int tp[]= MergeSort(t, new int[m], 0, m-1, 0, 0); System.out.println(tp[0]+" "+tp[1]);
							tp=MergeSort(t, new int[m], 0, m-1, 0, 0); System.out.println(tp[0]+" "+tp[1]);
							tp=MergeSort(t, new int[m], 0, m-1, 0, 1); System.out.println(tp[0]+" "+tp[1]); }
			}
		}
	}
	public static void BubbleSort(int t[], int n, int trace, int up){
		int i, j, primerjave=0, premike=0;
		if(trace==1) printT(t, n, -1, 0, 0);
		for(i=0; i<n-1; i++){
            for(j=n-1; j>i; j--){
                primerjave++;
				if(up==1 && t[j-1]>t[j] || up==0 && t[j-1]<t[j]){
					swap(t, j, j-1); premike+=3;
                }
            }
			if(trace==1) printT(t, n, i, 1, 0);
		}
		if(trace==0) System.out.println(primerjave+" "+premike);
	}
	public static void SelectionSort(int[] t, int n, int trace, int up){
		int i, j, index, primerjave=0, premike=0;
		if(trace==1) printT(t, n, -1, 0, 0);
		for(i=0; i<n-1; i++){
			index=i;
			for(j=i+1; j<n; j++){
				primerjave++;
				if(up==1 && t[j]<t[index] || up==0 && t[j]>t[index]) index=j;
			}
			if(i!=index) swap(t, i, index); premike+=3;
			if(trace==1) printT(t, n, i, 1, 0);
		}
		if(trace==0) System.out.println(primerjave+" "+premike);
	}
	public static void InsertionSort(int t[], int n, int trace, int up){
		int i, j, primerjave=0, premike=0;
		if(trace==1) printT(t, n, 0, 1, 0);
        for(i=1; i<n; i++){
            for(j=i; j>0; j--){
				primerjave++;
                if(up==1 && t[j]<t[j-1] || up==0 && t[j]>t[j-1]){
					swap(t, j, j-1); premike+=3;
                }else break;
            }
			if(trace==1) printT(t, n, i, 1, i==n-1?1:0);
        }
		if(trace==0) System.out.println(primerjave+" "+premike);
	}
	public static void HeapSort(int t[], int n, int trace, int up){
		int tp[]=new int[2]; //primerjave, premike
		for(int i=n/2; i>=0; i--){
			int tp2[]=heapify(t, i, n, trace, up);
			tp[0]+=tp2[0]; tp[1]+=tp2[1];
		}
		if(trace==1) printHS(t, n+1);
		
		while(n>0){
			swap(t, 0, n); tp[1]+=3;
			n--;
			int tp2[]=heapify(t, 0, n, trace, up);
			tp[0]+=tp2[0]; tp[1]+=tp2[1];
			if(trace==1) printHS(t, n+1);
		}
		if(trace==0) System.out.println(tp[0]+" "+tp[1]);
	}
	public static int[] heapify(int t[], int parent, int tableLength, int trace, int up){
		int child=2*parent+1, tp[]=new int[2]; //left child, primerjave, premike
		if(child<=tableLength){
			tp[0]++;
			if(child+1<=tableLength){
				tp[0]++;
				if(up==1 && t[child]<t[child+1] || up==0 && t[child]>t[child+1]) child+=1;
			} //right child
			if(up==1 && t[parent]<t[child] || up==0 && t[parent]>t[child]){
				swap(t, parent, child); tp[1]+=3;
				int tp2[]=heapify(t, child, tableLength, trace, up);
				tp[0]+=tp2[0]; tp[1]+=tp2[1];
			}
		}
		return tp;
	}
	public static int[] QuickSort(int[] t, int low, int high, int trace, int up){
		if(low>=high) return new int[2];
		int pivot=t[(low+high)/2], i=low, j=high, tp[]=new int[2];
		tp[1]++;
		while(i<=j){
			tp[0]+=2;
			while(up==1 && t[i]<pivot || up==0 && t[i]>pivot){ i++; tp[0]++; }
			while(up==1 && t[j]>pivot || up==0 && t[j]<pivot){ j--; tp[0]++; }
			if(i<=j){ swap(t, i, j); i++; j--; tp[1]+=3; }
		}
		if(trace==1) printQS(t, low, high, i, j+1);
		if(low<j){ int tp2[]=QuickSort(t, low, j, trace, up); tp[0]+=tp2[0]; tp[1]+=tp2[1]; }
		if(high>i){ int tp2[]=QuickSort(t, i, high, trace, up); tp[0]+=tp2[0]; tp[1]+=tp2[1]; }
		return tp;
	}
	public static int[] MergeSort(int t[], int tmp[], int left, int right, int trace, int up){
		int tp[]=new int[2];
		if(left<right){
			int middle=(left+right)/2;
			if(trace==1) printMS(t, left, right, 1);
			tp=MergeSort(t, tmp, left, middle, trace, up);
			if(trace==1) printMS(t, left, middle, 0);
			int tp2[]=MergeSort(t, tmp, middle+1, right, trace, up); tp[0]+=tp2[0]; tp[1]+=tp2[1];
			if(trace==1) printMS(t, middle+1, right, 0);
			tp2=merge(t, tmp, left, middle+1, right, up); tp[0]+=tp2[0]; tp[1]+=tp2[1];
		}else
			tp[1]++;
		return tp;
	}
	public static int[] merge(int t[], int tmp[], int left, int right, int rightEnd, int up){
        int leftEnd=right-1, k=left, tmp2=rightEnd-left+1, i, tp[]=new int[2];
        while(left<=leftEnd && right<=rightEnd){
            tp[0]++; tp[1]++;
			if(up==1 && t[left]<=t[right] || up==0 && t[left]>=t[right]) tmp[k++]=t[left++];
			else tmp[k++]=t[right++];
		}
        while(left<=leftEnd){ tmp[k++]=t[left++]; tp[1]++; }
        while(right<=rightEnd){ tmp[k++]=t[right++]; tp[1]++; }
		for(i=0; i<tmp2; i++){ t[rightEnd]=tmp[rightEnd]; rightEnd--; }
		return tp;
	}
	public static void printT(int t[], int n, int i, int s, int k){
		if(s==0) System.out.print("| ");
		for(int j=0; j<n; j++){
			if(s==1 && i+1==j) System.out.print("| ");
			System.out.print(t[j]+" ");
		}
		if(k==1) System.out.println("| ");
		else System.out.println();
	}
	public static void printHS(int t[], int n){
		int level=1;
		for(int i=0; i<n; i++){
			if(i+1==Math.pow(2, level)){ level++; System.out.printf("| %d ", t[i]); }
			else System.out.printf("%d ", t[i]);
		}
		System.out.printf("\n");
	}
	public static void printQS(int t[], int n1, int n2, int i, int j){
		for(int k=n1; k<=n2; k++){
			if(k==i && k==j) System.out.printf("| | ");
			else if(k==i || k==j) System.out.printf("| ");
			System.out.printf("%d ", t[k]);
		}
		System.out.printf("\n");
	}
	public static void printMS(int t[], int n1, int n2, int x){
		if(n1!=n2){
			int c=(n1+n2)/2;
			for(int i=n1; i<=n2; i++){
				System.out.printf("%d ", t[i]);
				if(x==1 && i==c) System.out.printf("| ");
			}
			System.out.printf("\n");
		}
	}
	public static void swap(int t[], int x, int y){
		int tmp = t[x]; t[x] = t[y]; t[y] = tmp;
	}
}