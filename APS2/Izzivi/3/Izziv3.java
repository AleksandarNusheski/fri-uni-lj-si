import java.util.Scanner;

public class Izziv3{
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		int n=Integer.parseInt(args[0]);
		int a[]=new int[n];
		for(int i=0; i<n; i++)
			a[i]=input.nextInt();
		input.close();

		n--;
		for(int i=n/2; i>=0; i--)
			pogrezni(a, i, n);
		
		while(n>=0){
			izpisi(a, n+1);
			zamenjaj(a, 0, n);
			n--;
			pogrezni(a, 0, n);
		}
    }
	
	public static void pogrezni(int a[], int i, int dolzKopice){
		int sin=2*i+1;
		if(sin<=dolzKopice){
			if(sin+1<=dolzKopice && a[sin]<a[sin+1]) sin++;
			if(a[i]<a[sin]){
				zamenjaj(a, i, sin);
				pogrezni(a, sin, dolzKopice);
			}
		}
	}
	
	public static void izpisi(int a[], int n){
		int nivo=1;
		for(int i=0; i<n; i++){
			if(i+1==Math.pow(2, nivo)){ nivo++; System.out.printf("| %d ", a[i]); }
            else System.out.printf("%d ", a[i]);
		}
		System.out.printf("\n");
	}
	
	public static void zamenjaj(int a[], int i, int j){
		int tmp=a[i]; a[i]=a[j]; a[j]=tmp;
	}
}