import java.util.*;

class Item{
	int volumen, cost;
	
	Item(int v, int c){
		volumen=v;
		cost=c;
	}
}

class LinkedListElement {
	Object element;
	LinkedListElement next;
	
	LinkedListElement(Object obj, LinkedListElement nxt){
		element = obj;
		next = nxt;
	}
}

class LinkedList{
	protected LinkedListElement first, last;
	
	LinkedList(){
		makenull();
	}
	
	public void makenull(){
		first = new LinkedListElement(null, null);
		last = null;
	}
	
	public void addLast(Object obj){
		LinkedListElement newEl = new LinkedListElement(obj, null);
		if (last == null){
			first.next = newEl;
			last = first;
		}else{
			last.next.next = newEl;
			last = last.next;
		}
	}
	
	public void addProperPlace(Object obj){
		LinkedListElement el, newEl = new LinkedListElement(obj, null);
		
		el=first;
		while(true){
			if(el==null || el.next==null) break;
			Item i1=(Item)el.next.element, i2=(Item)obj;
			if(i1.volumen>i2.volumen) break;
			el=el.next;
		}
		newEl.next = el.next;
		el.next = newEl;
		
		if (last == null) //ce smo dodali edini element
			last = first;
		else if (last == el) //ce smo dodali predzadnji element
			last = last.next;
		else if (last.next == el) //ce smo dodali zadnji element
			last = el;
		//v ostalih primerih se kazalec "last" ne spreminja
	}
	
	//Funkcija deleteNth izbrise element na n-tem mestu v seznamu
	//(prvi element seznama, ki se nahaja takoj za glavo seznama, je na indeksu 0)
	boolean deleteNth(int n){
		LinkedListElement el, prev_el;
		//zacnemo pri glavi seznama
		prev_el = null;
		el = first;
		//premaknemo se n-krat
		for (int i = 0; i < n; i++){
			prev_el = el;
			el = el.next;
			if (el == null) return false;
		}
		if (el.next != null){
			//preden izlocimo element preverimo, ali je potrebno popraviti kazalec "last"
			if (last == el.next) //ce brisemo predzadnji element
				last = el;
			else if (last == el) //ce brisemo zadnji element
				last = prev_el;
			el.next = el.next.next;
			return true;
		}
		else return false;
	}
	
	public void write(){
		LinkedListElement el;
		el = first.next;
		while(el != null){
			Item i1=(Item)el.element;
			System.out.print("("+i1.volumen+", "+i1.cost+") ");
			el = el.next;
		}
		System.out.println();
	}
	
	public void addition(Item t[], int i, LinkedList list2){
		LinkedListElement el=first.next;
		while(el!=null){
			Item i1=(Item)el.element;
			list2.addLast(new Item(i1.volumen+t[i].volumen, i1.cost+t[i].cost));
			el = el.next;
		}
	}
	
	public void join(LinkedList list1){
		LinkedListElement el=first.next;
		while(el!=null){
			list1.addProperPlace(el.element);
			el = el.next;
		}
	}
	
	public void removeInvalid(LinkedList list2, int V){
		int t[][]=new int[10][2], c=0;
		LinkedListElement el1, el2=list2.first.next;
		while(el2!=null){
			Item i2=(Item)el2.element;
			int k=0;
			el1=first.next;
			while(el1!=null){
				Item i1=(Item)el1.element;
				el1=el1.next;
				if(i2.volumen<=i1.volumen && i2.cost>=i1.cost){
					int w=1;
					for(int q=0; q<c; q++)
						if(t[q][0]==i1.volumen && t[q][1]==i1.cost) w=0;
					if(w==1){ t[c][0]=i1.volumen; t[c][1]=i1.cost; c++; }
//					System.out.println("Odstranimo ("+i1.volumen+", "+i1.cost+")");
					this.deleteNth(k);
				}else if(i1.volumen>V){
					this.deleteNth(k);
				}
				k++;
			}
			el2=el2.next;
		}
		
		el1=first.next;
		while(el1!=null){
			Item i1=(Item)el1.element;
			int k=0;
			el2=list2.first.next;
			while(el2!=null){
				Item i2=(Item)el2.element;
				el2=el2.next;
				if(i1.volumen<=i2.volumen && i1.cost>=i2.cost){
					int w=1;
					for(int q=0; q<c; q++)
						if(t[q][0]==i1.volumen && t[q][1]==i1.cost) w=0;
					if(w==1){ t[c][0]=i2.volumen; t[c][1]=i2.cost; c++; }
//					System.out.println("Odstranimo ("+i2.volumen+", "+i2.cost+")");
					list2.deleteNth(k);
				}else if(i2.volumen>V){
					list2.deleteNth(k);
				}
				k++;
			}
			el1=el1.next;
		}
		
		int tmp;
		for(int i=0; i<c; i++)
			for(int j=i+1; j<c; j++)
				if(t[j][0]<t[i][0]){
					tmp = t[j][0];
					t[j][0] = t[i][0];
					t[i][0] = tmp;
					tmp = t[j][1];
					t[j][1] = t[i][1];
					t[i][1] = tmp;
				}
		for(int i=0; i<c; i++)
			System.out.println("Odstranimo ("+t[i][0]+", "+t[i][1]+")");
	}
}

public class Izziv9{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		int i, j, k, V;
		Item t[]=new Item[Integer.parseInt(args[0])];
		for(i=0; i<t.length; i++)
			t[i]=new Item(input.nextInt(), -1);
		for(i=0; i<t.length; i++)
			t[i].cost=input.nextInt();
		V=input.nextInt();
		
		LinkedList list1=new LinkedList(), list2=new LinkedList();
		list1.addLast(new Item(0, 0));
		
		for(i=0; i<t.length; i++){
			list1.addition(t, i, list2);
			System.out.print(i+": ");
			list1.write();
			list1.removeInvalid(list2, V);
			
			list2.join(list1);
			list2=new LinkedList();
		}
		list1.write();
	}
}