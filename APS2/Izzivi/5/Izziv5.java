import java.util.Random;

class Izziv5{
	static int iter_min, iter_max, iter_comp, rek_min, rek_max, rek_comp, rek_izracun;

	public static void main(String[] args){
		Random r = new Random();
		double log2=Math.log(2);
		int i, n=128;
		
		for(int j=0; j<10; j++){
			int t[]=new int[n];
			iter_min=Integer.MAX_VALUE; iter_max=Integer.MIN_VALUE; iter_comp=0; rek_comp=0; rek_izracun=0;
			
			for(i=0; i<n; i++){
				t[i]=r.nextInt();
				iter_comp+=2;
				if(t[i]>iter_max) iter_max=t[i];
				if(t[i]<iter_min) iter_min=t[i];
			}
			
			long startTime = System.nanoTime();
			rek_min=rekurzijaMin(t, 0, n-1); rek_max=rekurzijaMax(t, 0, n-1);
			long executionTime = System.nanoTime() - startTime;
			
			int a=(int)(Math.log(n)/log2);
			rek_izracun=n;
			for(i=0; i<a; i++) rek_izracun/=2;
			rek_izracun+=(int)executionTime;
			
			System.out.printf("dolzina_tabele=%d, iter_min=%d, iter_max=%d, iter_comp=%d,\nrek_min=%d, rek_max=%d, rek_comp=%d, rek_izracun=%d\n\n", n, iter_min, iter_max, iter_comp, rek_min, rek_max, rek_comp, rek_izracun);
			n*=2;
		}
	}
	
	public static int rekurzijaMin(int[] t, int low, int high){
		if(low<high){
			int mid=(low+high)/2;
			int min1=rekurzijaMin(t, low, mid);
			int min2=rekurzijaMin(t, mid+1, high);
			rek_comp++;
			return min1<min2 ? min1 : min2;
		}
		return t[low];
	}
	public static int rekurzijaMax(int[] t, int low, int high){
		if(low<high){
			int mid=(low+high)/2;
			int max1=rekurzijaMax(t, low, mid);
			int max2=rekurzijaMax(t, mid+1, high);
			rek_comp++;
			return max1>max2 ? max1 : max2;
		}
		return t[low];
	}
}