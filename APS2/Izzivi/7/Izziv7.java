import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.Comparator;

class Network{
	Node[] nodes;
	
	/**
	 * Create a new network with n nodes (0..n-1).
	 * @param n the size of the network.
	 */
	public Network(int n){
		nodes = new Node[n];
		for (int i = 0; i < nodes.length; i++) {
			nodes[i]= new Node(i);
		}
	}
	/**
	 * Add a connection to the network, with all the corresponding in and out edges.
	 * @param fromNode
	 * @param toNode
	 * @param capacity
	 */
	public void addConnection(int fromNode, int toNode, int capacity){
		Edge e = new Edge(fromNode, toNode, capacity);
		nodes[fromNode].outEdges.add(e);
		nodes[toNode].inEdges.add(e);
	}

	/**
	 * Reset all the marks of the algorithm, before the start of a new iteration.
	 */
	public void resetMarks(){
		for (int i = 0; i < nodes.length; i++) {
			nodes[i].marked = false;
			nodes[i].augmEdge = null;
			nodes[i].incFlow = Integer.MAX_VALUE; //-1;
			nodes[i].direction = '*';
		}
	}
}

class Node{
	int id;
	//marks for the algorithm
	//------------------------------------
	char direction = '*';
	boolean marked = false;
	Edge augmEdge = null; //the edge over which we brought the flow increase
	int incFlow = Integer.MAX_VALUE; //-1; //-1 means a potentially infinite flow
	//------------------------------------
	ArrayList<Edge> inEdges;
	ArrayList<Edge> outEdges;
	
	public Node(int i) {
		id = i;
		inEdges = new ArrayList<Edge>();
		outEdges = new ArrayList<Edge>();
	}
}

class Edge{
	int startID;
	int endID;
	int capacity;
	int currFlow;
	
	public Edge(int fromNode, int toNode, int capacity2) {
		startID = fromNode;
		endID = toNode;
		capacity = capacity2;
		currFlow = 0;
	}
}

public class Izziv7 {
	public static void main(String[] args) {
		int numOfNodes=Integer.parseInt(args[0]);
		Network n=new Network(numOfNodes);
		Scanner input = new Scanner(System.in);
		
		while(input.hasNext()){
			n.addConnection(input.nextInt(), input.nextInt(), input.nextInt());
		}
		
		Queue<Node> q=new PriorityQueue<Node>(20, new NodeIdComparator());
		while(true){
			n.nodes[0].marked=true;
			q.add(n.nodes[0]);
			
			int sink=0;
			while (!q.isEmpty()){
				Node v = q.remove();
				
				for(int i=0; i<v.outEdges.size(); i++){
					if(!n.nodes[v.outEdges.get(i).endID].marked && v.outEdges.get(i).currFlow<v.outEdges.get(i).capacity){
						q.add(n.nodes[v.outEdges.get(i).endID]);
						n.nodes[v.outEdges.get(i).endID].marked=true;
						n.nodes[v.outEdges.get(i).endID].augmEdge=v.outEdges.get(i);
						n.nodes[v.outEdges.get(i).endID].incFlow=Math.min(v.incFlow, v.outEdges.get(i).capacity-v.outEdges.get(i).currFlow);
						n.nodes[v.outEdges.get(i).endID].direction='+';
						if(n.nodes[v.outEdges.get(i).endID]==n.nodes[numOfNodes-1]) sink=1;
					}
				}
				
				for(int i=0; i<v.inEdges.size(); i++){
					if(!n.nodes[v.inEdges.get(i).startID].marked && v.inEdges.get(i).currFlow>0){
						q.add(n.nodes[v.inEdges.get(i).startID]);
						n.nodes[v.inEdges.get(i).startID].marked=true;
						n.nodes[v.inEdges.get(i).startID].augmEdge=v.inEdges.get(i);
						n.nodes[v.inEdges.get(i).startID].incFlow=Math.min(v.incFlow, v.inEdges.get(i).currFlow);
						n.nodes[v.inEdges.get(i).startID].direction='-';
						if(n.nodes[v.inEdges.get(i).startID]==n.nodes[numOfNodes-1]) sink=1;
					}
				}
				
				if(sink==1){
					v=n.nodes[numOfNodes-1];
					System.out.print(v.incFlow+": ");
					do{
						System.out.print(v.id+""+v.direction+" ");
						if(v.direction=='+'){
							v.augmEdge.currFlow+=n.nodes[numOfNodes-1].incFlow; //v.incFlow;
							v=n.nodes[v.augmEdge.startID];
						}else if(v.direction=='-'){
							v.augmEdge.currFlow-=n.nodes[numOfNodes-1].incFlow; //v.incFlow;
							v=n.nodes[v.augmEdge.endID];
						}
					}while(v.id!=0);
					System.out.println("0");
					break;
				}
			}
			
			if(sink==0) break;
			
			while(!q.isEmpty()) q.remove();
			n.resetMarks();
		}
	}
}

class NodeIdComparator implements Comparator<Node>{
    @Override
    public int compare(Node x, Node y){
        if(x.id < y.id){
            return -1;
        }
        if(x.id > y.id){
            return 1;
        }
        return 0;
    }
}