import java.util.Random;

class Izziv2{
	public static void main(String[] args){
		System.out.println("   n     |    linearno  |   dvojisko   |\n---------+--------------+---------------");
		for(int i=1000; i<=100000; i+=1000)
			System.out.printf("%8d | %12d | %12d\n", i, timeLinear(i), timeBisection(i));
	}
	public static long timeLinear(int n){
		int t[]=generateTable(n);
		Random r = new Random();
		long startTime = System.nanoTime();
		for(int i=0; i<1000; i++)
			findLinear(t, r.nextInt(n)+1);
		long executionTime = System.nanoTime() - startTime;
		executionTime/=1000;
		return executionTime;
	}
	public static long timeBisection(int n){
		int t[]=generateTable(n);
		Random r = new Random();
		long startTime = System.nanoTime();
		for(int i=0; i<1000; i++)
			findBisection(t, 0, n, r.nextInt(n)+1);
		long executionTime = System.nanoTime() - startTime;
		executionTime/=1000;
		return executionTime;
	}
	public static int findLinear(int[] a, int v){
		for(int i=0; i<a.length; i++)
			if(a[i]==v) return i;
		return -1;
	}
	public static int findBisection(int[] a, int l, int r, int v){
		if(r<l) return -1;
		int c=(l+r)/2;
		if(a[c]>v) return findBisection(a, l, c-1, v);
		if(a[c]<v) return findBisection(a, c+1, r, v);
		return c;
	}
	public static int[] generateTable(int n){
		int t[]=new int[n];
		for(int i=0; i<n; i++)
			t[i]=i+1;
		return t;
	}
}