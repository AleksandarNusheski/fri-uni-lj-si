import java.util.ArrayList;
import java.util.Scanner;

class Network{
	Node[] nodes;
	
	/**
	 * Create a new network with n nodes (0..n-1).
	 * @param n the size of the network.
	 */
	public Network(int n){
		nodes = new Node[n];
		for (int i = 0; i < nodes.length; i++) {
			nodes[i]= new Node(i);
		}
	}
	/**
	 * Add a connection to the network, with all the corresponding in and out edges.
	 * @param fromNode
	 * @param toNode
	 * @param cost
	 */
	public void addConnection(int fromNode, int toNode, int cost){
		Edge e = new Edge(fromNode, toNode, cost);
		nodes[fromNode].outEdges.add(e);
		nodes[toNode].inEdges.add(e);
	}
}

class Node{
	int id;
	ArrayList<Edge> inEdges;
	ArrayList<Edge> outEdges;
	
	public Node(int i) {
		id = i;
		inEdges = new ArrayList<Edge>();
		outEdges = new ArrayList<Edge>();
	}
}

class Edge{
	int startID;
	int endID;
	int cost;
	
	public Edge(int fromNode, int toNode, int cost2) {
		startID = fromNode;
		endID = toNode;
		cost = cost2;
	}
}

public class Izziv10 {
	public static void main(String[] args) {
		int numOfNodes=Integer.parseInt(args[0]);
		Network n=new Network(numOfNodes);
		Scanner input = new Scanner(System.in);
		
		while(input.hasNext())
			n.addConnection(input.nextInt(), input.nextInt(), input.nextInt());
		
		int distance[][] = new int[numOfNodes][numOfNodes];
		for(int i=0; i<numOfNodes; i++){
			for(int j=0; j<numOfNodes; j++){
				if(j==0) distance[i][j]=0;
				else distance[i][j]=Integer.MAX_VALUE;
			}
		}
		
		System.out.print("h0: ");
		for(int i=0; i<numOfNodes; i++){
			if(distance[0][i]==Integer.MAX_VALUE) System.out.print("Inf ");
			else System.out.printf("%d ", distance[0][i]);
		}
		System.out.printf("\n");
		
		for(int h=1; h<numOfNodes; h++){
			for(int v=0; v<numOfNodes; v++){
				for(int e=0; e<n.nodes[v].inEdges.size(); e++){
					if(distance[h-1][n.nodes[v].inEdges.get(e).startID]!=Integer.MAX_VALUE)
						distance[h][v] = Math.min(distance[h][v], Math.min(distance[h-1][v], distance[h-1][n.nodes[v].inEdges.get(e).startID] + n.nodes[v].inEdges.get(e).cost));
					else
						distance[h][v] = Math.min(distance[h][v], Math.min(distance[h-1][v], Integer.MAX_VALUE));
				}
			}
			System.out.printf("h%d: ", h);
			for(int i=0; i<numOfNodes; i++){
				if(distance[h][i]==Integer.MAX_VALUE) System.out.print("Inf ");
				else System.out.printf("%d ", distance[h][i]);
			}
			System.out.printf("\n");
		}
	}
}
