class Izziv8{
	public static void main(String[] args){
		
		int n, k, x, N=Integer.parseInt(args[0])+1, K=Integer.parseInt(args[1])+1;
		int s[][]=new int[N][K];
		
		for(n=0; n<N; n++) s[n][1]=n;
		for(k=0; k<K; k++){ s[0][k]=0; s[1][k]=1; }
		
		for(n=2; n<N; n++){
			for(k=2; k<K; k++){
				int minimum=Integer.MAX_VALUE;
				for(x=1; x<=n; x++)
					minimum=Math.min(minimum, Math.max(s[x-1][k-1], s[n-x][k]));
				s[n][k]=1+minimum;
			}
		}
		for(n=0; n<N; n++){
			for(k=1; k<K; k++)
				System.out.printf("%4d", s[n][k]);
			System.out.printf("\n");
		}
	}
}