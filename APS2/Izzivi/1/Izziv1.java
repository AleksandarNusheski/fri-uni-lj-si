import java.util.Scanner;

class Izziv1{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int i, t[] = new int[Integer.parseInt(args[0])];
		for(i=0; i<t.length; i++) t[i]=input.nextInt();
		if(binarySearch(t, 0, t.length-1, input.nextInt())) System.out.println("Found");
		else System.out.println("Not found");
	}
	
	public static boolean binarySearch(int table[], int i, int j, int element){
		if(j<i) return false;
		int k, center=(i+j)/2;
		for(k=i; k<=j; k++){
			if(k!=center) System.out.printf("%d ", table[k]);
			else System.out.printf("|%d| ", table[k]);
		}
		System.out.println();
		if(table[center]>element) return binarySearch(table, i, center-1, element);
		if(table[center]<element) return binarySearch(table, center+1, j, element);
		return true;
	}
}