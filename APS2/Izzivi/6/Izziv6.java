import java.util.Scanner;

class Complex{
	double re;
	double im;
	 
    public Complex(double real, double imag) {
        re = real;
        im = imag;
    }
    
    public String toString() {
    	double tRe = (double)Math.round(re * 100000) / 100000;
    	double tIm = (double)Math.round(im * 100000) / 100000;
        if (tIm == 0) return tRe + "";
        if (tRe == 0) return tIm + "i";
        if (tIm <  0) return tRe + "-" + (-tIm) + "i";
        return tRe + "+" + tIm + "i";
    }

    // sestevanje 
    public Complex plus(Complex b) {
        Complex a = this;
        double real = a.re + b.re;
        double imag = a.im + b.im;
        return new Complex(real, imag);
    }

    // odstevanje
    public Complex minus(Complex b) {
        Complex a = this;
        double real = a.re - b.re;
        double imag = a.im - b.im;
        return new Complex(real, imag);
    }

    // mnozenje z drugim kompleksnim stevilo
    public Complex times(Complex b) {
        Complex a = this;
        double real = a.re * b.re - a.im * b.im;
        double imag = a.re * b.im + a.im * b.re;
        return new Complex(real, imag);
    }

    // mnozenje z realnim stevilom
    public Complex times(double alpha) {
        return new Complex(alpha * re, alpha * im);
    }

    // reciprocna vrednost kompleksnega stevila
    public Complex reciprocal() {
        double scale = re*re + im*im;
        return new Complex(re / scale, -im / scale);
    }

    // deljenje
    public Complex divides(Complex b) {
        Complex a = this;
        return a.times(b.reciprocal());
    }

    // e^this
    public Complex exp() {
        return new Complex(Math.exp(re) * Math.cos(im), Math.exp(re) * Math.sin(im));
    }
    
    //potenca komplesnega stevila
    public Complex pow(int k) {
    	Complex c = new Complex(1,0);
    	for (int i = 0; i < k ; i++) {
			c = c.times(this);
		}
    	return c;
    }
}

class Izziv6{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int i, n=Integer.parseInt(args[0]);
		int tmp=(int)(Math.log(n)/Math.log(2));
		if((int)Math.pow(2, tmp)!=n) tmp++;
		int m=(int)Math.pow(2, tmp);
		
		Complex t[]=new Complex[m];
		for(i=0; i<n; i++)
			t[i]=new Complex(input.nextInt(), 0);
		for( ; i<m; i++)
			t[i]=new Complex(0, 0);
		
		FFT(t);
	}
	
	public static Complex[] FFT(Complex a[]){
		int n=a.length;
		if(n==1) return a;
		
		Complex tmp1[]=new Complex[n/2], tmp2[]=new Complex[n/2];
		for(int i=0, j=0; i<n-1; i+=2){
			tmp1[j]=a[i]; tmp2[j++]=a[i+1];
		}
		Complex ys[]=FFT(tmp1); Complex yl[]=FFT(tmp2);
		
		Complex w=new Complex(0, 2*Math.PI/n).exp(), wk=new Complex(1, 0), y[]=new Complex[n];
		for(int k=0; k<n/2; k++){
			y[k]=ys[k].plus(wk.times(yl[k]));
			y[k+n/2]=ys[k].minus(wk.times(yl[k]));
			wk=wk.times(w);
		}
		for(int i=0; i<y.length; i++)
			System.out.print(y[i].toString()+" ");
		System.out.println();
		
		return y;
	}
}