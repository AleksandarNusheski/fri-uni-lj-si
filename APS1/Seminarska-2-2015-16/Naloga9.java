import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Naloga9 {
	public static void obdelaj(String vhodnaDatoteka, String izhodnaDatoteka){
		BufferedReader reader = null; BufferedWriter writer = null;
		try{
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			
			String tmp1, line, t1[]={".", "-", "..", "-.", ".-", "...", "--", "-..", ".-.", "..-", "....", "--.", "-.-", ".--", "-...", ".-..", "..-.", "...-", ".....", "---", "--..", "-.-.", "-..-", ".--.", ".-.-", "..--"};
			char tmp2, t2[]={'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
			int i, j, tmp3, t3[]=new int[26];

			while((line=reader.readLine())!=null)
				for(i=0; i<line.length(); i++){
					if(line.charAt(i)>=65 && line.charAt(i)<=90) t3[line.charAt(i)-'A']++;
					else if(line.charAt(i)>=97 && line.charAt(i)<=122) t3[line.charAt(i)-'a']++;
				}
			for(i=0; i<26; i++)
				for(j=i+1; j<26; j++)
					if(t3[j]>t3[i]){
						tmp3=t3[j]; t3[j]=t3[i]; t3[i]=tmp3;
						tmp2=t2[j]; t2[j]=t2[i]; t2[i]=tmp2;
					}
			for(i=0; i<26; i++)
				for(j=i+1; j<26; j++)
					if(t2[j]<t2[i]){
						tmp1=t1[j]; t1[j]=t1[i]; t1[i]=tmp1;
						tmp2=t2[j]; t2[j]=t2[i]; t2[i]=tmp2;
					}
			for(i=0; i<26; i++)
				writer.write(t2[i]+":"+t1[i]+"\r\n");
		
		}catch (FileNotFoundException ex){
		}catch (IOException ex){
		}catch (NumberFormatException ex){
		}catch (Exception ex){
		}finally {
			try {
				if(reader!=null) reader.close();
				if(writer!=null) writer.close();
			}catch (IOException ex) {}
		}
	}
	
	public static String vrniVpisnoStevilko(){
		return "63130344";
	}
}