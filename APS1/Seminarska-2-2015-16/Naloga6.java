import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Queue;
import java.util.LinkedList;
import java.lang.Math;

class Node {
	int value, type, sum;
	Node left, right, parent;
	
	public Node(int v, Node p) {
		value=v;
		type=0;
		parent=p;
	}
}

public class Naloga6 {
	public static int sumOfElements(Node node){
		if(node==null) return 0;
		return sumOfElements(node.left)+node.value+sumOfElements(node.right);
	}
	public static int maxElement(Node node) {
		if(node==null) return Integer.MIN_VALUE;
		return Math.max(maxElement(node.left), Math.max(node.value, maxElement(node.right)));
	}
	public static int minElement(Node node) {
		if(node==null) return Integer.MAX_VALUE;
		return Math.min(minElement(node.left), Math.min(node.value, minElement(node.right)));
	}
	public static int height(Node node) {
		if(node==null) return 0;
		return 1+Math.max(height(node.left), height(node.right));
	}
	public static void obdelaj(String vhodnaDatoteka, String izhodnaDatoteka){
		BufferedReader reader = null; BufferedWriter writer = null;
		try{
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			String t[]=reader.readLine().split(",");
			int i, difference;
			Node root=new Node(Integer.parseInt(t[0]), null), current=null;
			current=root;
			
			for(i=1; i<t.length; ) {
				if(t[i].charAt(0)!='0'){
					if(current.type==0){
						current.left=new Node(Integer.parseInt(t[i++]), current);
						current.type++;
						current=current.left;
					}else{
						current.right=new Node(Integer.parseInt(t[i++]), current);
						current.type++;
						current=current.right;
					}
				}else{
					i++;
					current.type++;
					while(current!=root && current.type>1) current=current.parent;
				}
			}
			Queue<Node> queue=new LinkedList<Node>();
			queue.add(root);
			while(!queue.isEmpty()){
				Node tempNode=queue.poll();
				if(tempNode.left!=null) tempNode.left.sum=sumOfElements(tempNode.left);
				if(tempNode.right!=null) tempNode.right.sum=sumOfElements(tempNode.right);
				if(tempNode.left==null && tempNode.right==null) difference=0;
				else if(tempNode.left==null) difference=tempNode.right.sum;
				else if(tempNode.right==null) difference=0-tempNode.left.sum;
				else difference=tempNode.right.sum-tempNode.left.sum;
				writer.write((tempNode==root ? sumOfElements(tempNode) : tempNode.sum)+","+minElement(tempNode)+","+ maxElement(tempNode)+","+height(tempNode)+","+difference+"\r\n");
				if(tempNode.left!=null) queue.add(tempNode.left);
				if(tempNode.right!=null) queue.add(tempNode.right);
			}
		}catch (FileNotFoundException ex){
		}catch (IOException ex){
		}catch (NumberFormatException ex){
		}catch (Exception ex){
		}finally {
			try {
				if(reader!=null) reader.close();
				if(writer!=null) writer.close();
			}catch (IOException ex) {}
		}
	}
	public static String vrniVpisnoStevilko(){
		return "63130344";
	}
}