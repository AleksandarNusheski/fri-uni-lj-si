import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

class Element{
	Object elm;
	Element next;

	Element(){
		elm = null;
		next = null;
	}
}
class Queue{
	// nove elemente dodajamo za element 'rear', elemente jemljemo s front strani
	private Element front;
	private Element rear;
	
	public Queue(){
		makenull();
	}
	
	public void makenull(){
		front = null;
		rear = null;
	}
	
	public void setFront(Element f){
		front=f;
	}
	
	public void setRear(Element r){
		rear=r;
	}
	
	public Element getFront(){
		return front;
	}
	
	public Element getRear(){
		return rear;
	}
	
	public boolean empty(){
		return (front == null);
	}
	
	public Object front(){
		if (!empty()) return front.elm;
		return null;
	}
	
	public void enqueue(Object obj){
		Element el = new Element();
		el.elm = obj;
		el.next = null;
		if (empty()) front = el;
		else rear.next = el;		
		rear = el;
	}
	
	public void dequeue(){
		if (!empty()){
			front = front.next;
			if (front == null) rear = null;
		}
	}
	
	public void writeIt(String izhodnaDatoteka){
		BufferedWriter writer = null;
		try{
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			Element el=front;
			while(el != null){
				if(el.next == null) writer.write(el.elm+"");
				else writer.write(el.elm+",");
				el = el.next;
			}
			writer.write("\r\n");
		}catch (FileNotFoundException ex){
		}catch (IOException ex){
        }catch (NumberFormatException ex){
		}catch (Exception ex){
		}finally {
			try {
				if(writer!=null) writer.close();
			}catch (IOException ex) {}
		}
	}
}

public class Karte{
	public static void obdelaj(String vhodnaDatoteka, String izhodnaDatoteka){
		BufferedReader reader = null;
		try{
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			String D, I, vhod; int n, S;
			n=Integer.parseInt(reader.readLine().split(" ")[1]);
			vhod=reader.readLine();
			
			Queue kup1=new Queue(), kup2=new Queue();
			for(String retval: vhod.split(","))
				kup1.enqueue(retval);
			
			for(int j=0; j<n; j++){
				String[] splitStr = reader.readLine().split(" ");
				D=splitStr[0]; I=splitStr[1]; S=Integer.parseInt(splitStr[2]);
				
				Element el=kup1.getFront();
				while(true){
					if(el==null){
						kup2.setFront(kup1.getFront());
						kup2.setRear(kup1.getRear());
						kup1=new Queue();
						break;
					}
					if(el.elm.equals(D)){
						kup2.setFront(el.next);
						kup2.setRear(kup1.getRear());
						el.next=null;
						kup1.setRear(el);
						break;
					}
					el=el.next;
				}
				
				Element pointer1, pointer2;
				el=kup1.getFront();
				while(true){
					if(el==null){
						pointer1=null;
						pointer2=null;
						break;
					}
					if(el.elm.equals(I)){
						pointer1=el;
						pointer2=el;
						break;
					}
					el=el.next;
				}
				
				if(pointer1!=null){
					while(!kup2.empty()){
						for(int i=0; i<S; i++){
							el=new Element();
							el.elm=kup2.front();
							if(el.elm==null) break;
							kup2.dequeue();
							el.next=pointer2.next;
							pointer2.next=el;
							pointer2=pointer2.next;
						}
						pointer2=pointer1;
					}
				}else{
					while(!kup2.empty()){
						for(int i=0; i<S; i++){
							el=new Element();
							el.elm=kup2.front();
							if(el.elm==null) break;
							kup2.dequeue();
							if(i==0){
								el.next=kup1.getFront();
								kup1.setFront(el);
								pointer2=el;
							}else{
								el.next=pointer2.next;
								pointer2.next=el;
								pointer2=pointer2.next;
							}
						}
					}
				}
			}
			kup1.writeIt(izhodnaDatoteka);
		}catch (FileNotFoundException ex){System.out.println(ex);
		}catch (IOException ex){System.out.println(ex);
        }catch (NumberFormatException ex){System.out.println(ex);
		}catch (Exception ex){System.out.println(ex);
		}finally {
			try {
				if(reader!=null) reader.close();
			}catch (IOException ex) {System.out.println(ex);}
		}
	}
	
	public static void main(String[] args){
		int stPonovitev = 1;
		long zacetniCas = System.nanoTime();

		for (int i = 0; i < stPonovitev; i++){
			for(int j=1; j<=10; j++){
				String vhod = String.format("./Naloga3-Karte-tests/Vhodi/I3_%d.txt", j);
				String izhod = String.format("./Naloga3-Karte-tests/MojeResitve/MO3_%d.txt", j);
				obdelaj(vhod,izhod);
			}
		}
		
		double povCasIzvajanja = (double)(System.nanoTime() - zacetniCas) / stPonovitev;
		System.out.println("Povprecen cas izvajanja: " + povCasIzvajanja + "\n");
	}
	
	public static String vrniVpisnoStevilko(){
		return "63130344";
	}
}
