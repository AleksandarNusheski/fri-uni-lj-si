import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;


class Product{
	int velikost, cena, uporabnoDni;
	String ime;
	Product nextProduct;
	
	public Product(){}
	public Product(String ime, int velikost, int cena, int uporabnoDni){
		this.ime=ime;
		this.velikost=velikost;
		this.cena=cena;
		this.uporabnoDni=uporabnoDni;
	}
	
	public String toString(){
		return String.format("%s,%d,%d,%d\r\n", ime, velikost, cena, uporabnoDni);
	}
}

class Shelf{
	Shelf nextShelf;
	int dolzina, currentDolzina, numberOfProducts;
	Product firstProduct;
	
	public Shelf(){
		firstProduct = new Product();
	}
	
	public Shelf(int dolzina) {
		this.dolzina=dolzina;
		currentDolzina=0;
		firstProduct = new Product();
		numberOfProducts=0;
	}
	
	public void addProduct(String ime, int velikost, int cena, int uporabnoDni){
		Product currentProduct=firstProduct;
		while (currentProduct.nextProduct != null){
			if(currentProduct.nextProduct.uporabnoDni > uporabnoDni) break;
			else currentProduct=currentProduct.nextProduct;
		}
		Product newProduct=new Product(ime, velikost, cena, uporabnoDni);
		newProduct.nextProduct=currentProduct.nextProduct;
		currentProduct.nextProduct=newProduct;
		currentDolzina+=velikost;
		numberOfProducts++;
	}
	public void checkAndRemoveProducts(int steviloDni){
		Product el = firstProduct;
		while (el.nextProduct != null){
			el.nextProduct.uporabnoDni-=steviloDni;
			if(el.nextProduct.uporabnoDni<1){
				currentDolzina-=el.nextProduct.velikost;
				el.nextProduct=el.nextProduct.nextProduct;
				numberOfProducts--;
			}else el = el.nextProduct;
		}
	}
	
	public String write(){
		Product el = firstProduct.nextProduct;
		String result=numberOfProducts+"\r\n";
		while (el != null){
			result+=el.toString();
			el = el.nextProduct;
		}
		return result;
	}
}

public class Trgovina{
	protected Shelf firstShelf, lastShelf;
	int numberOfShelfs;
	
	public Trgovina() {
		firstShelf = new Shelf();
		lastShelf = null;
		numberOfShelfs=0;
	}
	
	public void dodajPolico(int dolzina) {
		Shelf newEl = new Shelf(dolzina);
		if (lastShelf == null){
			firstShelf.nextShelf = newEl;
			lastShelf = firstShelf;
		}else{
			lastShelf.nextShelf.nextShelf = newEl;
			lastShelf = lastShelf.nextShelf;
		}
		numberOfShelfs++;
	}
	public boolean dodajIzdelek(int polica, String ime, int velikost, int cena, int uporabnoDni) {
		Shelf tmpEl = firstShelf.nextShelf;
		for(int i=0; i<polica; i++){
			if(tmpEl==null) return false;
			tmpEl=tmpEl.nextShelf;
		}
		if(tmpEl.currentDolzina+velikost<=tmpEl.dolzina){
			tmpEl.addProduct(ime, velikost, cena, uporabnoDni);
			return true;
		}
		return false;
	}
	public void posodobi(int steviloDni) {
		Shelf tmpEl = firstShelf.nextShelf;
		while(tmpEl!=null){
			tmpEl.checkAndRemoveProducts(steviloDni);
			tmpEl=tmpEl.nextShelf;
		}
	}
	public void izpis(String izhodnaDatoteka) {
		BufferedWriter writer = null;
		try{
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			writer.write(numberOfShelfs+"\r\n");
			Shelf el = firstShelf.nextShelf;
			while(el != null){
	//			System.out.println("Shelf "+i+" maxDolzina="+el.dolzina+" currentDolzina="+el.currentDolzina+": ");
				writer.write(el.write());
				el = el.nextShelf;
			}
		}catch (FileNotFoundException ex){
		}catch (IOException ex){
	    }catch (NumberFormatException ex){
		}catch (Exception ex){
		}finally {
			try {
				if(writer!=null) writer.close();
			}catch (IOException ex) {}
		}
	}
	public static String vrniVpisnoStevilko(){
		return "63130344";
	}
	public static void main(String args[]) {
		Trgovina t = new Trgovina();
		t.dodajPolico(10);
		t.dodajIzdelek(0, "a", 1, 10, 5);
		t.dodajIzdelek(0, "b", 3, 20, 3);
		t.dodajIzdelek(0, "c", 2, 40, 4);
		t.posodobi(3);
		t.izpis("izhod.txt");
	}
}