import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

class Tile {
	boolean right, down, left, up;
	char type, degrees;

	public Tile() {
		right = true;
		down = false;
		left = false;
		up = true;
		type = 'C';
		degrees = '1';
	}

	void transformCtoB() {
		right = false;
		up = true;
		left = false;
		down = true;
		type = 'B';
		degrees = '1';
	}

	void rotate90() {
		boolean tmp = right;
		right = up;
		up = left;
		left = down;
		down = tmp;
		degrees++;
	}
}

public class Cikel {
	static int v, s, solution;
	static Tile t[][];
	
	public static void obdelaj(String vhodnaDatoteka, String izhodnaDatoteka) {
		BufferedReader reader = null;BufferedWriter writer = null;
		try{
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			String vhod[]=reader.readLine().split(",");
			
			int a, b, c, vMin, vMax, sMin, sMax;
			a = Integer.parseInt(vhod[0]); b = Integer.parseInt(vhod[1]); c = Integer.parseInt(vhod[2]);
			v = Integer.parseInt(vhod[3]); s = Integer.parseInt(vhod[4]);
			
			t = new Tile[s*2-1][v*2-1];
			Tile tile = new Tile();
			t[s-1][v-1]=tile; c--;
			vMin=s-1; sMin=s-1;
			vMax=s-1; sMax=s-1;
			
			solution=0;
			//right
			mtd(b, c, s, v-1, vMin, vMax+1, sMin, sMax, izhodnaDatoteka);
			if(solution==0) writer.write("-1\r\n");
		}catch (FileNotFoundException ex){
		}catch (IOException ex){
		}catch (NumberFormatException ex){
		}catch (Exception ex){
		}finally {
			try {
				if(reader!=null) reader.close();
				if(writer!=null) writer.close();
			}catch (IOException ex){}
		}
	}

	public static void mtd(int b, int c, int x, int y, int vMin, int vMax, int sMin, int sMax, String izhodnaDatoteka){
		if(solution==1 || Math.abs(sMax-sMin)+1>v || Math.abs(vMax-vMin)+1>s) return;
		
		int valid, flag;
		Tile tile=new Tile();
		for(int l=1; l<7; l++){
			if(b==0 && (l==5 || l==6)) continue;
			if(c==0 && l>=1 && l<=4){ if(l==4) tile.transformCtoB(); continue; }
			
			valid=0; flag=0;
			//down
			if(y+1<v*2-1){
				if(t[x][y+1]==null) valid++;
				else if(t[x][y+1]!=null && tile.down==t[x][y+1].up) valid++;
				else flag=1;
			}else if(y+1==v*2-1 && tile.down==false) valid++;
			else flag=2;
			
			if(flag==0){
				//right
				if(x+1<s*2-1){
					if(t[x+1][y]==null) valid++;
					else if(t[x+1][y]!=null && tile.right==t[x+1][y].left) valid++;
					else flag=3;
				}else if(x+1==s*2-1 && tile.right==false) valid++;
				else flag=4;

				if(flag==0){
					//up
					if(y-1>-1){
						if(t[x][y-1]==null) valid++;
						else if(t[x][y-1]!=null && tile.up==t[x][y-1].down) valid++;
						else flag=5;
					}else if(y-1==-1 && tile.up==false) valid++;
					else flag=6;
					
					if(flag==0){
						//left
						if(x-1>-1){
							if(t[x-1][y]==null) valid++;
							else if(t[x-1][y]!=null && tile.left==t[x-1][y].right) valid++;
							else flag=7;
						}else if(x-1==-1 && tile.left==false) valid++;
						else flag=8;
					}
				}
			}
			
			if(valid==4){
				if(l>=1 && l<=4) c--; else b--;
				t[x][y]=tile;
				
				if(b==0 && c==0 && t[s-1][v-2]!=null && t[s-1][v-1].up==t[s-1][v-2].down){
					print(vMin, vMax, sMin, sMax, izhodnaDatoteka);
					solution=1; return;
				}else{
					if(tile.right && x+1<s*2-1 && t[x+1][y]==null)
						mtd(b, c, x+1, y, vMin, x+1>vMax ? vMax+1 : vMax, sMin, sMax, izhodnaDatoteka);
					if(tile.up && y-1>-1 && t[x][y-1]==null)
						mtd(b, c, x, y-1, vMin, vMax, y-1<sMin ? sMin-1 : sMin, sMax, izhodnaDatoteka);
					if(tile.left && x-1>-1 && t[x-1][y]==null)
						mtd(b, c, x-1, y, x-1<vMin ? vMin-1 : vMin, vMax, sMin, sMax, izhodnaDatoteka);
					if(tile.down && y+1<v*2-1 && t[x][y+1]==null)
						mtd(b, c, x, y+1, vMin, vMax, sMin, y+1>sMax ? sMax+1 : sMax, izhodnaDatoteka);
				}
				if(l>=1 && l<=4) c++; else b++;
				t[x][y]=null;
			}
			if(l==4) tile.transformCtoB();
			else tile.rotate90();
		}
	}

	public static void print(int vMin, int vMax, int sMin, int sMax, String izhodnaDatoteka){
		BufferedWriter writer = null;
		try{
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			int tmpV=0, tmpS=0;
			for(int j=sMin; j<=sMax; j++){
				tmpS=0;
				tmpV++;
				for(int i=vMin; i<=vMax; i++){
					tmpS++;
					if(t[i][j]!=null) writer.write(t[i][j].type+","+t[i][j].degrees+",");
					else writer.write("A,1,");
				}
				for(int i=tmpS; i<s; i++)
					writer.write("A,1,");
			}
			for(int j=tmpV; j<v; j++)
				for(int i=0; i<s; i++)
					writer.write("A,1,");
			writer.write("\r\n");
		}catch (FileNotFoundException ex){
		}catch (IOException ex){
        }catch (NumberFormatException ex){
		}catch (Exception ex){
		}finally {
			try {
				if(writer!=null) writer.close();
			}catch (IOException ex) {}
		}
	}
	
	public static String vrniVpisnoStevilko() {
		return "63130344";
	}
}
