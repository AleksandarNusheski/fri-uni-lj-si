import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class CikelTestSlika extends JPanel {
	String[] vzorec;
	int polozaj[], x, y;
	int size = 50; // velikost enega kvadratka
	
	public static void main(String[] args) {
		int inputTest=10;
		String vhodnaDatoteka = String.format("./Naloga5-Cikel-tests/Vhodi/I5_%d.txt", inputTest);
		String izhodnaDatoteka = String.format("./Naloga5-Cikel-tests/MojeResitve/MO5_%d.txt", inputTest);
		Cikel.obdelaj(vhodnaDatoteka, izhodnaDatoteka);
		JFrame frame = new JFrame();
		frame.getContentPane().add(new CikelTestSlika(izhodnaDatoteka, vhodnaDatoteka));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 700);
		frame.setVisible(true);
	}
	
	void narisiOkvir(Graphics g) {
		g.drawRect(10, 10, x*size, y*size);
		for (int i = 0; i < x; i++) {
			g.drawLine(i*size+10, 10, i*size+10, size*y+10);
		}
		for (int i = 0; i < y; i++) {
			g.drawLine(10, i*size+10, size*x+10, i*size+10);
		}
	}

	public void paint(Graphics g) {
		narisiOkvir(g);
		Graphics2D g2d = (Graphics2D)g;
		int width = 6;
		g2d.setStroke(new BasicStroke(width));
		
		for (int j = 0; j < y; j++) {
			for (int i = 0; i < x; i++) {
				int tmpX = i*size+10;
				int tmpY = j*size+10;
				char vzorc = vzorec[j*x+i].charAt(0);

				switch (vzorc) {
				case 'A':
					break;
				case 'B':
					if (polozaj[j*x+i] == 1) {
						g2d.drawLine(tmpX+size/2, tmpY+width/2, tmpX+size/2, tmpY+size-width/2);
					}
					else if (polozaj[j*x+i] == 2) {
						g2d.drawLine(tmpX+width/2, tmpY+size/2, tmpX+size-width/2, tmpY+size/2);
					}
					else {
						System.out.println("Narobe maš!!! Vzorec B mora biti na položaju 1 ali 2...");
						g2d.drawLine(tmpX, tmpY, tmpX+size-width/2, tmpY+size);
						g2d.drawLine(tmpX+size-width/2, tmpY, tmpX, tmpY+size-width/2);
					}
					break;
				case 'C':
					if (polozaj[j*x+i] == 1) {
						g2d.drawLine(tmpX+size/2, tmpY+width/2, tmpX+size/2, tmpY+size/2);
						g2d.drawLine(tmpX+size/2, tmpY+size/2, tmpX+size-width/2, tmpY+size/2);
					}
					else if (polozaj[j*x+i] == 2) {
						g2d.drawLine(tmpX+size/2, tmpY+size-width/2, tmpX+size/2, tmpY+size/2);
						g2d.drawLine(tmpX+size/2, tmpY+size/2, tmpX+size-width/2, tmpY+size/2);
					}
					else if (polozaj[j*x+i] == 3) {
						g2d.drawLine(tmpX+size/2, tmpY+size-width/2, tmpX+size/2, tmpY+size/2);
						g2d.drawLine(tmpX+width/2, tmpY+size/2, tmpX+size/2, tmpY+size/2);
					}
					else if (polozaj[j*x+i] == 4) {
						g2d.drawLine(tmpX+width/2, tmpY+size/2, tmpX+size/2, tmpY+size/2);
						g2d.drawLine(tmpX+size/2, tmpY+width/2, tmpX+size/2, tmpY+size/2);
					}
					else {
						System.out.println("Narobe maš!!! Vzorec C mora biti na položaju med 1 in 4...");
						g2d.drawLine(tmpX, tmpY, tmpX+size-width/2, tmpY+size);
						g2d.drawLine(tmpX+size-width/2, tmpY, tmpX, tmpY+size-width/2);
					}
					break;
				}
			}
		}
	}
	
	public CikelTestSlika(String vhodnaDatoteka, String podatki) {
		try {
			Scanner sc = new Scanner(new File(vhodnaDatoteka));
			String vhod = sc.nextLine();
			sc = new Scanner(new File(podatki));
			String data = sc.nextLine();
			if (vhod.equals("")) {
				System.out.println("Program sploh nič ni napisal!!!");
			}
			String[] dat = data.split(",");
			sc.close();
			
			String[] vse = vhod.split(",");
			vzorec = new String[vse.length/2];
			polozaj = new int[vse.length/2];
			this.x = Integer.parseInt(dat[4]);
			this.y = Integer.parseInt(dat[3]);
			
			for (int i = 0; i < vse.length; i++) {
				if (i % 2 == 0) {
					vzorec[i/2] = vse[i];
				} else {
					polozaj[i/2] = Integer.parseInt(vse[i]);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public CikelTestSlika(String podatki, int y, int x) {
		String[] vse = podatki.split(",");
		vzorec = new String[vse.length/2];
		polozaj = new int[vse.length/2];
		this.x = x;
		this.y = y;
		
		for (int i = 0; i < vse.length; i++) {
			if (i % 2 == 0) {
				vzorec[i/2] = vse[i];
			} else {
				polozaj[i/2] = Integer.parseInt(vse[i]);
			}
		}
	}
}
