import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

class Card{
	int value;
	char sign;
	
	public Card(int value, char sign){
		this.value=value;
		this.sign=sign;
	}
}

class Player{
	Card t[];
	int playerID, counter;

	Player(int id){
		t=new Card[5];
		playerID=id;
		counter=0;
	}
}

public class Igra{
	public static void obdelaj(String vhodnaDatoteka, String izhodnaDatoteka){
		BufferedReader reader = null; BufferedWriter writer = null;
		try{
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			int i, j, k, l, players=Integer.parseInt(reader.readLine().split(" ")[0]), wins[]=new int[players];
			String vhod[]=reader.readLine().split(",");
			Player p[]=new Player[players];
			
			for(i=0; i<players; i++)
				p[i]=new Player(i);
			for(i=0; i<players*5; i++)
				p[i%players].t[p[i%players].counter++]=new Card(Integer.parseInt(vhod[i].substring(0, vhod[i].length()-1)), vhod[i].charAt(vhod[i].length()-1));
			
			Card maxCI=new Card(-2, '*'), maxC, minC;
			int pi=0, pj=0, maxIndex=-1, minIndex=-1, maxCIindex=-1, noMoreCards=0, vhodC=players*5;
			for(i=0; i<vhod.length; i++){
				pi=pi%players;
				maxC=new Card(-1, '-');
				minC=new Card(32770, '-');
				if(pj==0){
					for(j=0; j<5; j++){
						if(p[pi].t[j].value==-1) continue;
						if(p[pi].t[j].value>maxC.value){
							maxC.value=p[pi].t[j].value;
							maxC.sign=p[pi].t[j].sign;
							maxIndex=j;
						}else if(p[pi].t[j].value==maxC.value && p[pi].t[j].sign<maxC.sign){
							maxC.sign=p[pi].t[j].sign;
							maxIndex=j;
						}
						if(p[pi].t[j].value<minC.value){
							minC.value=p[pi].t[j].value;
							minC.sign=p[pi].t[j].sign;
							minIndex=j;
						}else if(p[pi].t[j].value==minC.value && p[pi].t[j].sign<minC.sign){
							minC.sign=p[pi].t[j].sign;
							minIndex=j;
						}
					}
				}else{
					for(j=0; j<5; j++){
						if(p[pi].t[j].value==-1) continue;
						if(p[pi].t[j].sign==maxCI.sign && p[pi].t[j].value>maxCI.value){
							if(p[pi].t[j].sign!=maxC.sign || p[pi].t[j].sign==maxC.sign && p[pi].t[j].value>maxC.value){
								maxC.value=p[pi].t[j].value;
								maxC.sign=p[pi].t[j].sign;
								maxIndex=j;
							}
						}
						if(p[pi].t[j].value<minC.value){
							minC.value=p[pi].t[j].value;
							minC.sign=p[pi].t[j].sign;
							minIndex=j;
						}else if(p[pi].t[j].value==minC.value && p[pi].t[j].sign<minC.sign){
							minC.sign=p[pi].t[j].sign;
							minIndex=j;
						}
					}
				}
				if(pj==0){
					maxCI.value=maxC.value;
					maxCI.sign=maxC.sign;
					p[pi].t[maxIndex]=new Card(-1, '+');
					maxCIindex=pi;
				}else{
					if(maxC.value>maxCI.value){
						maxCI.value=maxC.value;
						maxCI.sign=maxC.sign;
						p[pi].t[maxIndex]=new Card(-1, '+');
						maxCIindex=pi;
					}else{
						p[pi].t[minIndex]=new Card(-1, '+');
					}
				}
				pi++; pj++;
				if(i!=0 && pj%players==0){
					pi=maxCIindex;
					wins[maxCIindex]++;
					pj=0;
					if(noMoreCards==0){
						l=maxCIindex;
						for(j=0; j<players; j++){
							l=l%players;
							for(k=0; k<5; k++){
								if(p[l].t[k].value!=-1) continue;
								p[l].t[k]=new Card(Integer.parseInt(vhod[vhodC].substring(0, vhod[vhodC].length()-1)), vhod[vhodC].charAt(vhod[vhodC].length()-1));
								break;
							}
							vhodC++; l++;
						}
						if(vhodC==vhod.length) noMoreCards=1;
					}
				}
			}
			String result="";
			for(i=0; i<players; i++)
				result+=wins[i]+"\r\n";
			writer.write(result);
		}catch (FileNotFoundException ex){
		}catch (IOException ex){
        }catch (NumberFormatException ex){
		}catch (Exception ex){
		}finally {
			try {
				if(reader!=null) reader.close();
				if(writer!=null) writer.close();
			}catch (IOException ex) {}
		}
	}
	
	public static String vrniVpisnoStevilko(){
		return "63130344";
	}
}