import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

class ListNode 
{
	Object element;
	ListNode next;
	
	public ListNode(Object obj, ListNode nextNode) 
	{
		element = obj;
		next = nextNode;
	}
}

class LinkedList 
{
	private ListNode first;
	private int size;
	
	public LinkedList() 
	{
		makenull();
	}
	
	public void makenull() 
	{
		first = new ListNode(null, null);
		size = 0;
	}
	
	public ListNode first()
	{
		return first;
	}
	
	public ListNode next(ListNode pos)
	{
		return pos.next;
	}
	
	public Object retrieve(ListNode pos)
	{
		return pos.next.element;
	}
	
	public boolean overEnd(ListNode pos)
	{
		if (pos.next == null)
			return true;
		else
			return false;
	}
	
	public void add(Object element) 
	{
		first.next = new ListNode(element, first.next);
		size++;
	}
	
	public LinkedList makeCopy()
	{
		LinkedList newList = new LinkedList();
		
		for (ListNode curNode = first(); !overEnd(curNode); curNode = next(curNode))
		{
			newList.add(retrieve(curNode));
		}
		
		return newList;
	}
	
	public int getSize()
	{
		return size;
	}
	
	public void print()
	{
		for (ListNode curNode = first(); !overEnd(curNode); curNode = next(curNode))
		{
			System.out.print(retrieve(curNode) + ", ");
		}
		
		System.out.println();
	}
}


class QueueElement
{
	Object element;
	QueueElement next;

	QueueElement()
	{
		element = null;
		next = null;
	}
}

class Queue
{
	//QueueElement -> QueueElement -> QueueElement -> ... -> QueueElement
	//    front                                                   rear
	//
	// nove elemente dodajamo na rear strani
	// elemente jemljemo s front strani
	
	private QueueElement front;
	private QueueElement rear;
	
	public Queue()
	{
		makenull();
	}
	
	public void makenull()
	{
		front = null;
		rear = null;
	}
	
	public boolean empty()
	{
		return (front == null);
	}
	
	public Object front()
	{
		if (!empty())
			return front.element;
		else
			return null;
	}
	
	public void enqueue(Object obj)
	{
		QueueElement el = new QueueElement();
		el.element = obj;
		el.next = null;
		
		if (empty())
		{
			front = el;
		}
		else
		{
			rear.next = el;
		}
		
		rear = el;
	}
	
	public void dequeue()
	{
		if (!empty())
		{
			front = front.next;
			
			if (front == null)
				rear = null;
		}
	}
}

class GraphVertex{
	Object value;
	GraphEdge firstEdge;
	GraphVertex nextVertex;
	
	public GraphVertex(Object val){
		value = val;
		firstEdge = null;
		nextVertex = null;
	}
	
	public String toString(){
		return value.toString();
	}
}

class GraphEdge{
	Comparable evalue;
	GraphVertex endVertex;
	GraphEdge nextEdge;
		
	public GraphEdge(Comparable eval, GraphVertex eVertex, GraphEdge nEdge){
		evalue = eval;
		endVertex = eVertex;
		nextEdge = nEdge;
	}
}

class DirectedGraph {
	protected GraphVertex fVertex;
	
	public void makenull(){
		fVertex = null;
	}
	
	public void insertVertex(GraphVertex v){
		v.nextVertex = fVertex;
		fVertex = v;
	}
	
	public void insertEdge(GraphVertex v1, GraphVertex v2, Comparable eval){
		GraphEdge newEdge = new GraphEdge(eval, v2, v1.firstEdge);
		v1.firstEdge = newEdge;
	}
	
	public GraphVertex firstVertex(){
		return fVertex;
	}
	
	public GraphVertex nextVertex(GraphVertex v){
		return v.nextVertex;
	}
	
	public GraphEdge firstEdge(GraphVertex v){
		return v.firstEdge;
	}
	
	public GraphEdge nextEdge(GraphVertex v, GraphEdge e){
		return e.nextEdge;
	}
	
	public GraphVertex endPoint(GraphEdge e){
		return e.endVertex;
	}
	
	public void print(){
		for (GraphVertex v = firstVertex(); v != null; v = nextVertex(v)) {
			System.out.print(v + ": ");
			for (GraphEdge e = firstEdge(v); e != null; e = nextEdge(v, e))
				System.out.print(endPoint(e) + "(" + e.evalue + ")" + ", ");
			System.out.println();
		}
	}
}

class Person{
	int distance;
	String name;
	
	public Person(String name){
		this.name = name;
	}
	
	public String toString(){
		return name;
	}
}

public class Prevoz {
	// Pomozna funkcija, ki poisce doloceno osebo v grafu
	public static GraphVertex findPerson(DirectedGraph diGraph, String name){
		for (GraphVertex curVertex = diGraph.firstVertex(); curVertex != null; curVertex = diGraph.nextVertex(curVertex)){
			// preveri, ali je to oseba, ki jo iscemo
			Person curPerson = (Person)curVertex.value;
			if (curPerson.name.equals(name)) return curVertex;
		}
		// osebe s tem imenom ni v grafu
		return null;
	}
	
	// Pomozna funkcija, ki inicializira atribute oseb v grafu
	public static void initPersons(DirectedGraph diGraph){
		for (GraphVertex curVertex = diGraph.firstVertex(); curVertex != null; curVertex = diGraph.nextVertex(curVertex)){
			Person curPerson = (Person)curVertex.value;
			curPerson.distance = 0;
		}
	}
		
	// Implementirajte metodo int veriga(DirectedGraph diGraph, String imeA, String imeB), 
	// ki vrne dolzino najkrajse "verige" poznanstev od osebe imeA do osebe imeB. 
	// Ce se osebi poznata, je dolzina verige 1. 
	// Ce se ne poznata, imata pa skupnega prijatelja, je dolzina verige 2 in tako naprej. 
	// Ce med osebama imeA in imeB ni poti v grafu poznanstev, funkcija vrne -1.
	public static int veriga(DirectedGraph diGraph, String imeA, String imeB){
		GraphVertex curVertex;
		Person curPerson;
		
		// vsem vozliscem nastavimo zacetno razdaljo na 0 
		initPersons(diGraph);
		
		// poiscemo zacetno in koncno osebo
		GraphVertex vertexA = findPerson(diGraph, imeA);
		GraphVertex vertexB = findPerson(diGraph, imeB);
		
		// ce osebi obstajata v grafu, zacnemo z iskanjem najkrajse verige poznanstev
		if (vertexA != null && vertexB != null){
			Queue q = new Queue();
			q.enqueue(vertexA);
			
			while (!q.empty()){
				curVertex = (GraphVertex)q.front();
				q.dequeue();
				
				curPerson = (Person)curVertex.value;
				if (curVertex == vertexB)
					return curPerson.distance;
				else{
					for (GraphEdge curEdge = diGraph.firstEdge(curVertex); curEdge != null; curEdge = diGraph.nextEdge(curVertex, curEdge)){
						GraphVertex nextVertex = diGraph.endPoint(curEdge);
						Person nextPerson = (Person)nextVertex.value;
						
						if (nextPerson.distance == 0){
							nextPerson.distance = curPerson.distance + 1;
							q.enqueue(nextVertex);
						}
					}
				}	
			}
		}
		return -1;
	}
	
	public static DirectedGraph createGraph1(String vhod[], int max){
		int i, j, k, tmpI, tmpJ;
		DirectedGraph diGraph = new DirectedGraph();
		GraphVertex[] vertices = new GraphVertex [max+1];
		for(k=0; k<vhod.length; k++){
			String[] vhod2=vhod[k].split(",");
			for(i = 0; i < vhod2.length-1; i++){
				for(j = i+1; j < vhod2.length; j++){
					tmpI=Integer.parseInt(vhod2[i]);
					tmpJ=Integer.parseInt(vhod2[j]);					
					if(vertices[tmpI]==null){
						vertices[tmpI]=new GraphVertex(new Person("Oseba"+tmpI));
						diGraph.insertVertex(vertices[tmpI]);
					}
					if(vertices[tmpJ]==null){
						vertices[tmpJ]=new GraphVertex(new Person("Oseba"+tmpJ));
						diGraph.insertVertex(vertices[tmpJ]);
					}
					diGraph.insertEdge(vertices[tmpI], vertices[tmpJ], "p");
					diGraph.insertEdge(vertices[tmpJ], vertices[tmpI], "p");
				}
			}
		}
		return diGraph;
	}
	
	public static DirectedGraph createGraph2(String vhod[], int max){
		int i, k, tmpI, tmpIplus1;
		DirectedGraph diGraph = new DirectedGraph();
		GraphVertex[] vertices = new GraphVertex [max+1];
		for(k=0; k<vhod.length; k++){
			String[] vhod2=vhod[k].split(",");
			for (i = 0; i < vhod2.length-1; i++){
				tmpI=Integer.parseInt(vhod2[i]);
				tmpIplus1=Integer.parseInt(vhod2[i+1]);
				if(vertices[tmpI]==null){
					vertices[tmpI]=new GraphVertex(new Person("Oseba"+tmpI));
					diGraph.insertVertex(vertices[tmpI]);
				}
				if(vertices[tmpIplus1]==null){
					vertices[tmpIplus1]=new GraphVertex(new Person("Oseba"+tmpIplus1));
					diGraph.insertVertex(vertices[tmpIplus1]);
				}
				diGraph.insertEdge(vertices[tmpI], vertices[tmpIplus1], "p");
				diGraph.insertEdge(vertices[tmpIplus1], vertices[tmpI], "p");
			}
		}
		return diGraph;
	}
	
	public static void main(String[] args){
		BufferedReader reader = null; BufferedWriter writer = null;
		try{
			String vhodnaDatoteka = args[0];
			String izhodnaDatoteka = args[1];
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			
			String line=reader.readLine();
			int i, j, n=Integer.parseInt(line), max=Integer.MIN_VALUE, tmp;
			String[] vhod=new String[n];
			
			for(i=0; i<n; i++){
				vhod[i]=reader.readLine();
				String line2[]=vhod[i].split(",");
				for(j=0; j<line2.length; j++){
					tmp=Integer.parseInt(line2[j]);
					if(tmp>max) max=tmp;
				}
			}
			
			line=reader.readLine();
			String line2[]=line.split(",");
			
			DirectedGraph diGraph = createGraph1(vhod, max);
	//		diGraph.print();
			writer.write((veriga(diGraph, "Oseba"+line2[0], "Oseba"+line2[1])-1)+"\r\n");
			diGraph = createGraph2(vhod, max);
	//		diGraph.print();
			writer.write(veriga(diGraph, "Oseba"+line2[0], "Oseba"+line2[1])+"\r\n");
		}catch (FileNotFoundException ex){System.out.println(ex);
		}catch (IOException ex){System.out.println(ex);
        }catch (NumberFormatException ex){System.out.println(ex);
		}catch (Exception ex){System.out.println(ex);
		}finally {
			try {
				if(reader!=null) reader.close();
				if(writer!=null) writer.close();
			}catch (IOException ex) {System.out.println(ex);}
		}
	}
	
	public static String vrniVpisnoStevilko(){
		return "63130344";
	}
}
