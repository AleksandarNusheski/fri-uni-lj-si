import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

class BSTNode {
	int value, visited;
	BSTNode left, right, up;
	
	public BSTNode(int val) {
		value = val;
		left = null;
		right = null;
		up = null;
		visited=0;
	}
}

class BSTree {
	BSTNode root;
	private int minNode;
	int key1, key2, a, b, c, d;
	
	public BSTree() {
		makenull();
	}
	
	// Funkcija naredi prazno drevo
	void makenull() {
		root = null;
	}
	
	void setKeys(int k1, int k2){
		key1=k1;
		key2=k2;
	}
	
	void printKeys(){
		System.out.println("key1="+key1+" key2="+key2);
	}
	
	void resetValues(){
		a=0; b=0; c=0; d=0;
	}
	
	// Rekurzivna funkcija za izpis poddrevesa s podanim korenom
	private void write(BSTNode node) {
		if (node != null) {
			System.out.print("(");
			write(node.left);
			System.out.print(", " + node.value + ", ");
			write(node.right);
			System.out.print(")");
		}
		else
			System.out.print("null");
	}
	
	// Funkcija za izpis drevesa
	public void write() {
		write(root);
		System.out.println();
	}
	
	// Rekurzivna funkcija, ki izpise elemente podanega poddrevesa v padajocem vrstnem redu
	private void descending(BSTNode node) {
		if (node != null) {
			descending(node.right);
			System.out.print(node.value + ", ");
			descending(node.left);
		}
	}
	
	// Funkcija, ki izpise elemente drevesa v padajocem vrstnem redu
	public void descending() {
		descending(root);
		System.out.println();
	}
	
	// Rekurzivna funkcija, ki preveri, ali se podani element nahaja v podanem poddrevesu
	private boolean memberRek(BSTNode node, int v) {
		if (node == null) return false;
		else if (v == node.value) return true;
		else if (v%2==0 && Math.abs(node.value%2)==1) return memberRek(node.left, v);
		else if (Math.abs(v%2)==1 && node.value%2==0) return memberRek(node.right, v);
		else if (v<node.value) return memberRek(node.left, v);
		else if (v>node.value) return memberRek(node.right, v);
		else return false;
	}
	
	// Funkcija preveri, ali se podani element nahaja v drevesu
	public boolean memberRek(int v) {
		return memberRek(root, v);
	}
	
	private BSTNode getNode(BSTNode node, int v) {
		if (node == null) return null;
		else if (v == node.value) return node;
		else if (v%2==0 && Math.abs(node.value%2)==1) return getNode(node.left, v);
		else if (Math.abs(v%2)==1 && node.value%2==0) return getNode(node.right, v);
		else if (v<node.value) return getNode(node.left, v);
		else if (v>node.value) return getNode(node.right, v);
		else return null;
	}
	
	public BSTNode getNode(int v) {
		return getNode(root, v);
	}
	
	protected BSTNode rightRotation(BSTNode n){
		BSTNode tmp=n;
		n=n.left;
		tmp.left=n.right;
		n.right=tmp;
		return n;
	}
	
	protected BSTNode leftRotation(BSTNode n){
		BSTNode tmp=n;
		n=n.right;
		tmp.right=n.left;
		n.left=tmp;
		return n;
	}
	
	private BSTNode insertRoot(BSTNode node, int v){
		if (node == null) node = new BSTNode(v);
		else if (v%2==0 && Math.abs(node.value%2)==1){
			node.left = insertRoot(node.left, v);
			node=rightRotation(node);
		}else if (Math.abs(v%2)==1 && node.value%2==0){
			node.right = insertRoot(node.right, v);
			node=leftRotation(node);
		}else if (v < node.value){
			node.left = insertRoot(node.left, v);
			node=rightRotation(node);
		}else if (v > node.value){
			node.right = insertRoot(node.right, v);
			node=leftRotation(node);
		}else
			;//element je ze v drevesu, ne naredimo nicesar
		return node;
	}
	
	public void insertRoot(int v) {
		root = insertRoot(root, v);
	}
	
	public BSTNode delete(BSTNode node, int v){
		if(node!=null){
			if(v == node.value){
				if(node.left==null) node=node.right;
				else if(node.right==null) node=node.left;
				else{
					node.right=deleteMin(node.right);
					node.value=minNode;
				}
			}else if (v%2==0 && Math.abs(node.value%2)==1){
				node.left = delete(node.left, v);
			}else if (Math.abs(v%2)==1 && node.value%2==0){
				node.right = delete(node.right, v);
			}else if(v < node.value){
				node.left=delete(node.left, v);
			}else{
				node.right=delete(node.right, v);
			}
		}
		return node;
	}
	
	public void delete(int v) {
		root = delete(root, v);
	}
	
	private BSTNode deleteMin(BSTNode node){
		if(node.left!=null){
			node.left=deleteMin(node.left);
			return node;
		}else{
			minNode=node.value;
			return node.right;
		}
	}
	
	// Rekurzivna funkcija za vstavljanje elementa v list drevesa
	private BSTNode insertLeaf(BSTNode node, int v){
		if (node == null)
			node = new BSTNode(v);
		else if (v%2==0 && Math.abs(node.value%2)==1)
			node.left = insertLeaf(node.left, v);
		else if (Math.abs(v%2)==1 && node.value%2==0)
			node.right = insertLeaf(node.right, v);
		else if (v < node.value)
			node.left = insertLeaf(node.left, v);
		else if (v > node.value)
			node.right = insertLeaf(node.right, v);
		else
			;//element je ze v drevesu, ne naredimo nicesar
		return node;
	}
	
	// Rekurzivna funkcija za vstavljanje elementa v list drevesa
	public void insertLeaf(int v) {
		root = insertLeaf(root, v);
	}
	
	public void calcPathSum(BSTNode node, BSTNode lca){
		if(node.visited==0 && node.value!=key1 && node.value!=key2) a+=node.value;
		node.visited=1;
		if(node.value==lca.value) return;
		calcPathSum(node.up, lca);
	}
	
	public void calcAndLinkUpNodes(){
		calcAndLinkUpNodes(root);
	}
	
	public void calcAndLinkUpNodes(BSTNode node) {
		if(node==null) return;
		//
		if(node.left!=null && node.left.up==null) node.left.up=node;
		if(node.right!=null && node.right.up==null) node.right.up=node;
		//
		node.visited=0;
		
		
		calcAndLinkUpNodes(node.left);
		
        if((node.value%2==0 && Math.abs(key1%2)==1) || (Math.abs(node.value%2)==Math.abs(key1%2) && node.value<key1)){
        	b+=node.value; //System.out.print(node.value+" ");
        }
        
        if((key1%2==0 && Math.abs(node.value%2)==1 || Math.abs(node.value%2)==Math.abs(key1%2) && key1 < node.value)
        && (Math.abs(key2%2)==1 && node.value%2==0 || Math.abs(node.value%2)==Math.abs(key2%2) && node.value < key2)){
        	c+=node.value; //System.out.print(node.value+" ");
        }
        
        if((Math.abs(node.value%2)==1 && key2%2==0)
        || (Math.abs(node.value%2)==Math.abs(key2%2) && node.value>key2)){
        	d+=node.value; //System.out.print(node.value+" ");
        }
        
		calcAndLinkUpNodes(node.right);
	}
	
	public String printValues(){
//		System.out.printf("%d %d %d %d\n", a, b, c, d);
		return String.format("%d %d %d %d", a, b, c, d);
	}
}

class TemporaryLCA{
    public int count;
    public BSTNode node;
 
    public TemporaryLCA(int count, BSTNode node){
        this.count = count;
        this.node = node;
    }
}

public class BST{
    public static TemporaryLCA getLCAnode(BSTNode node, int k1, int k2){
        if(node == null) return new TemporaryLCA(0, null);
 
        TemporaryLCA left = getLCAnode(node.left, k1, k2);
        if(left.count==2) return left;
 
        TemporaryLCA right = getLCAnode(node.right, k1, k2);
        if(right.count==2) return right;
 
        return new TemporaryLCA((node.value==k1 || node.value==k2) ? left.count+right.count+1 : left.count+right.count, node);
    }
	
    public static BSTNode lowestCommonAncestor(BSTNode node, int k1, int k2) {
        return getLCAnode(node, k1, k2).node;
    }
    
	public static void main(String[] args) {
		BufferedReader reader = null; BufferedWriter writer = null;
		try{
			String vhodnaDatoteka = args[0];
			String izhodnaDatoteka = args[1];
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			String vhod=reader.readLine();
			
			BSTree tree = new BSTree();
			int minOdd=Integer.MAX_VALUE, maxEven=Integer.MIN_VALUE, allEven=1, tmp;
			for(String retval: vhod.split(" ")){
				tmp=Integer.parseInt(retval);
				if(tmp%2==0 && tmp>maxEven)
					maxEven=tmp;
				if(Math.abs(tmp%2)==1 && tmp<minOdd)
					minOdd=tmp;
				if(allEven==1 && Math.abs(tmp%2)==1) allEven=0;
				tree.insertLeaf(tmp);
//				System.out.println(tmp);
			}
//			System.out.println("min="+minOdd+" max="+maxEven);
//			tree.write();
			
			if(allEven==0){
				tree.delete(minOdd);
//				tree.write();
				tree.insertRoot(minOdd);
			}else{
				tree.delete(maxEven);
//				tree.write();
				tree.insertRoot(maxEven);
			}
//			tree.write();
			
			int k1, k2;
			BSTNode keyNode1, keyNode2, lca;
			while((vhod=reader.readLine())!=null){
				k1=Integer.parseInt(vhod.split(" ")[0]);
				k2=Integer.parseInt(vhod.split(" ")[1]);
				if(Math.abs(k1%2)==1 && k2%2==0 || Math.abs(k1%2)==Math.abs(k2%2) && k1>k2){ tmp=k1; k1=k2; k2=tmp; }
				tree.setKeys(k1, k2);
//				tree.printKeys();
				
				if(!tree.memberRek(k1)){
					writer.write("-1\r\n");
				}else{
					if(!tree.memberRek(k2)){
						writer.write("-1\r\n");
					}else{
						tree.resetValues();
						tree.calcAndLinkUpNodes();
						if(k1!=k2){
							keyNode1=tree.getNode(k1);
							keyNode2=tree.getNode(k2);
							lca=lowestCommonAncestor(tree.root, k1, k2);
							tree.calcPathSum(keyNode1, lca);
							tree.calcPathSum(keyNode2, lca);
						}
						writer.write(tree.printValues()+"\r\n");
					}
				}
			}			
		}catch (FileNotFoundException ex){System.err.println(ex);
		}catch (IOException ex){System.err.println(ex);
        }catch (NumberFormatException ex){System.err.println(ex);
		}catch (Exception ex){System.err.println(ex);
		}finally {
			try {
				if(reader!=null) reader.close();
				if(writer!=null) writer.close();
			}catch (IOException ex) {System.err.println(ex);}
		}
	}
}