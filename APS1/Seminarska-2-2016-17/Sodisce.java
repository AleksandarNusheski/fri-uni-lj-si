import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

class ArgumentNode {
	String ime;
	ArgumentNode oce, brat, sin;
	int flag;
	
	public ArgumentNode(String ime, ArgumentNode oce){
		this.ime = ime;
		this.oce = oce;
		flag=-1;
	}
}

class Courthouse {
	ArgumentNode koren;
	
	public Courthouse(String ime) {
		koren = new ArgumentNode(ime, null);
	}
	
	//Pomozna metoda: izpise poddrevo s korenom v vozliscu 'v'
	private void izpis(int zamik, ArgumentNode v, BufferedWriter writer) {
		try{
			for(int i = 0; i < zamik; i++)
				writer.write(" ");
			if(!v.ime.equals("-"))
				writer.write(v.ime+","+(v.flag==1?"S":"NS")+"\r\n");
			ArgumentNode sin = v.sin;
			while (sin != null) {
				izpis(zamik+1, sin, writer);
				sin = sin.brat;
			}
		}catch (IOException ex){
			System.out.println(ex);
		}catch (Exception ex){
			System.out.println(ex);
		}
	}
	
	public void izpis(BufferedWriter writer){
		izpis(-1, koren, writer);
	}
	
	//Pomozna metoda: vrne kazalec na vozlisce, ki vsebuje osebo s podanim imenom in se nahaja v poddrevesu s korenom 'v'
	private ArgumentNode poisci(String ime, ArgumentNode v) {
		if (v.ime.equals(ime)) return v;
		else {
			ArgumentNode sin = v.sin, r;
			while (sin != null) {
				r = poisci(ime, sin);
				if (r != null) return r;
				else sin = sin.brat;
			}
			return null;
		}
	}
	
	//Metoda doda sina podanemu ocetu
	public boolean dodajSina(String oce, String sin) {
		ArgumentNode v = poisci(oce, koren);
		if (v != null){
			ArgumentNode s = new ArgumentNode(sin, v), s2=v.sin;
			if(s2!=null){
				while(s2.brat!=null)
					s2=s2.brat;
				s2.brat=s;
			}else
				v.sin = s;
			return true;
		}else
			return false;
	}
	
	public void examine(ArgumentNode node){
		if(node.sin==null) node.flag=1;
		ArgumentNode sin = node.sin;
		while (sin != null) {
			examine(sin);
			if(sin.brat==null){
				ArgumentNode tmp=node.sin;
				int noneS=0;
				while(tmp != null){
					if(tmp.flag==1){ noneS=1; break; }
					tmp=tmp.brat;
				}
				if(noneS==0) node.flag=1;
				else node.flag=0;
			}
			sin = sin.brat;
		}
	}
	
	public void examine(){
		examine(koren);
	}
	
	public boolean guilty(ArgumentNode node){
		int existAcceptedLawyersArgument=0, existAcceptedProsecutorsArgument=0;
		ArgumentNode tmp=node.sin;
		while(tmp!=null){
			if(tmp.ime.charAt(0)=='O' && tmp.flag==1) existAcceptedLawyersArgument=1;
			if(tmp.ime.charAt(0)=='T' && tmp.flag==1) existAcceptedProsecutorsArgument=1;
			if(existAcceptedLawyersArgument==1 && existAcceptedProsecutorsArgument==1) break;
			tmp=tmp.brat;
		}
		return existAcceptedLawyersArgument==0 && existAcceptedProsecutorsArgument==1;
	}
	
	public boolean guilty(){
		return guilty(koren);
	}
}

public class Sodisce {
	public static void main(String[] args){
		BufferedReader reader = null; BufferedWriter writer = null;
		try{
			String vhodnaDatoteka = args[0];
			String izhodnaDatoteka = args[1];
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			
			Courthouse tree = new Courthouse("-");
			String vhod=reader.readLine();
			int i, n=Integer.parseInt(vhod);
			
			for(i=0; i<n; i++){
				vhod=reader.readLine();
				String splitted[]=vhod.split(",");
				if(splitted.length==1) tree.dodajSina("-", splitted[0]);
				else tree.dodajSina(splitted[1], splitted[0]);
			}
			tree.examine();
			tree.izpis(writer);
			if(tree.guilty()) writer.write("K\r\n");
			else writer.write("N\r\n");
		}catch (FileNotFoundException ex){System.out.println(ex);
		}catch (IOException ex){System.out.println(ex);
        }catch (NumberFormatException ex){System.out.println(ex);
		}catch (Exception ex){System.out.println(ex);
		}finally {
			try {
				if(reader!=null) reader.close();
				if(writer!=null) writer.close();
			}catch (IOException ex) {System.out.println(ex);}
		}
	}
}
