import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Queue;
import java.util.LinkedList;

class BSTNode {
	int value;
	BSTNode left;
	BSTNode right;
	
	public BSTNode(int val) {
		value = val;
		left = null;
		right = null;
	}
}

class BSTree {
	BSTNode root;
	
	public BSTree() {
		makenull();
	}
	
	// Funkcija naredi prazno drevo
	void makenull() {
		root = null;
	}
	
	// Rekurzivna funkcija za izpis poddrevesa s podanim korenom
	private void write(BSTNode node) {
		if (node != null) {
			System.out.print("(");
			write(node.left);
			System.out.print(", " + node.value + ", ");
			write(node.right);
			System.out.print(")");
		}
		else
			System.out.print("null");
	}
	
	// Funkcija za izpis drevesa
	public void write() {
		write(root);
		System.out.println();
	}
	
	private BSTNode insert(BSTNode node, int t[], int beginIndex, int endIndex){
		int i, max=Integer.MIN_VALUE, maxIndex=-1;
		for(i=beginIndex; i<endIndex; i++)
			if(t[i]>max){ max=t[i]; maxIndex=i; }
		if(maxIndex==-1) return null;
		
		node = new BSTNode(t[maxIndex]);
		node.left = insert(node.left, t, beginIndex, maxIndex);
		node.right = insert(node.right, t, maxIndex+1, endIndex);
		
		return node;
	}
	
	public void insert(int t[], int beginIndex, int endIndex) {
		root = insert(root, t, beginIndex, endIndex);
	}
}	

public class Rekonstrukcija {
	public static void main(String[] args) {
		BufferedReader reader = null; BufferedWriter writer = null;
		try{
			String vhodnaDatoteka = args[0], izhodnaDatoteka = args[1];
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			String vhod=reader.readLine();
		
			String retval[]=vhod.split(",");
			int i, n=retval.length, first=1;
			int t[]=new int[n];
			
			for(i=0; i<n; i++)
				t[i]=Integer.parseInt(retval[i]);
			
			BSTree tree=new BSTree();
			tree.insert(t, 0, n);
//			tree.write();
			
			Queue<BSTNode> queue = new LinkedList<BSTNode>();
	        queue.add(tree.root);
	        while (!queue.isEmpty()) {
	        	BSTNode tempNode = queue.poll();
	        	if(first==1){
	        		writer.write(""+tempNode.value);
	        		first=0;
	        	}else{
	        		writer.write(","+tempNode.value);
	        	}
	            if (tempNode.left != null) queue.add(tempNode.left);
	            if (tempNode.right != null) queue.add(tempNode.right);
	        }
	        writer.write("\r\n");
		}catch (FileNotFoundException ex){System.err.println(ex);
		}catch (IOException ex){System.err.println(ex);
        }catch (NumberFormatException ex){System.err.println(ex);
		}catch (Exception ex){System.err.println(ex);
		}finally {
			try {
				if(reader!=null) reader.close();
				if(writer!=null) writer.close();
			}catch (IOException ex) {System.err.println(ex);}
		}
	}
	public static String vrniVpisnoStevilko(){
		return "63130344";
	}
}
