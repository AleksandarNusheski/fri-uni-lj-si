import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Naloga2 {
	public static void obdelaj(String vhodnaDatoteka, String izhodnaDatoteka){
		BufferedReader reader = null; BufferedWriter writer = null;
		try{
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			
			int i, j, n, m, g=0, h=0, p=0;
			n = Integer.parseInt(reader.readLine());
			m = Integer.parseInt(reader.readLine());
			
			String t[][]=new String[m][3], split[]=new String[3];
			for(i=0; i<m; i++){
				split = reader.readLine().split(",");
				g=0; h=0;
				for(j=0; j<p; j++){
					if(t[j][0].equals(split[0]) && t[j][1].equals(split[1]) && t[j][2].equals(split[2])) g=1;
					if(t[j][0].equals(split[0]) && t[j][1].equals(split[1]) && !t[j][2].equals(split[2])) h=1;
				}
				if(h==1) break; if(g==1) continue;
				t[p][0]=split[0]; t[p][1]=split[1]; t[p][2]=split[2]; p++;
			}
			if(h==1) writer.write("0\r\n");
			else writer.write((long)(Math.pow(2, n)/Math.pow(2, p))+"\r\n");

		}catch (FileNotFoundException ex){
		}catch (IOException ex){
        }catch (NumberFormatException ex){
		}catch (Exception ex){
		}finally {
			try {
				if(reader!=null) reader.close();
				if(writer!=null) writer.close();
			}catch (IOException ex) {}
		}
	}
	
	public static String vrniVpisnoStevilko(){
		return "63130344";
	}
}