import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

class QueueElement{
	Object element;
	QueueElement next;

	QueueElement(){
		element = null;
		next = null;
	}
}
class Queue{
	//QueueElement -> QueueElement -> QueueElement -> ... -> QueueElement
	//    front                                                   rear
	//
	// nove elemente dodajamo za element 'rear'
	// elemente jemljemo s front strani
	private QueueElement front;
	private QueueElement rear;
	
	public Queue(){
		makenull();
	}
	
	public void makenull(){
		front = null;
		rear = null;
	}
	
	public boolean empty(){
		return (front == null);
	}
	
	public Object front(){
		if (!empty()) return front.element;
		else return null;
	}
	
	public void enqueue(Object obj){
		QueueElement el = new QueueElement();
		el.element = obj;
		el.next = null;
		
		if (empty()){front = el;}
		else{rear.next = el;}
		
		rear = el;
	}
	
	public void dequeue(){
		if (!empty()){
			front = front.next;
			
			if (front == null) rear = null;
		}
	}
}

public class Naloga1 {
	public static void obdelaj(String vhodnaDatoteka, String izhodnaDatoteka){
		BufferedReader reader = null; BufferedWriter writer = null;
		try{
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			String line = reader.readLine();
			int numOfLines = Integer.parseInt(line), sp=0, gt;
			Queue q[] = new Queue[numOfLines];
			String t[] = new String[numOfLines];
			
			for (int i=0; i<numOfLines; i++){
				line = reader.readLine();
				String[] splitStr = line.split(" ");
				if(splitStr[0].equals("ENQ")){
					String[] splitStr2 = splitStr[1].split(",");
					gt=1;
					for(int j=0; j<sp; j++)
						if(t[j].equals(splitStr2[0])){
							q[j].enqueue(splitStr2[1]); gt=0;
						}
					if(gt==1){
						q[sp]=new Queue(); q[sp].enqueue(splitStr2[1]);
						t[sp]=splitStr2[0]; sp++;
					}
				}else
					for(int j=0; j<sp; j++)
						if(t[j].equals(splitStr[1])){
							writer.write(q[j].front()+"\r\n");
							q[j].dequeue();
						}
			}
		}catch (FileNotFoundException ex){
		}catch (IOException ex){
        }catch (NumberFormatException ex){
		}catch (Exception ex){
		}finally {
			try {
				if(reader!=null) reader.close();
				if(writer!=null) writer.close();
			}catch (IOException ex) {}
		}
	}
	public static String vrniVpisnoStevilko(){
		return "63130344";
	}
}