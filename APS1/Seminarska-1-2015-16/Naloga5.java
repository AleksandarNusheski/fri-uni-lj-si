import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Naloga5 {
	static int v, s, u, stK, stFunkcij, tp[][]={{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
	
	public static void obdelaj(String datotekaLabirint, String datotekaProgram, int vrstica, int stolpec, int usmeritev, int steviloKorakov, String izhodnaDatoteka){
		BufferedReader reader1 = null, reader2 = null; BufferedWriter writer = null;
		try{
			v=vrstica; s=stolpec; u=usmeritev; stK=steviloKorakov;
			reader1 = new BufferedReader(new FileReader(datotekaLabirint));
			reader2 = new BufferedReader(new FileReader(datotekaProgram));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			
			int n, m, i, j, stUkazov; String line;
			line = reader1.readLine(); n = Integer.parseInt(line);
			line = reader1.readLine(); m = Integer.parseInt(line);
			
			char t[][] = new char[n][m];
			for(i=n-1; i>=0; i--){
				line = reader1.readLine();
				String[] split = line.split(",");
				for(j=0; j<m; j++)
					t[i][j]=split[j].charAt(0);
			}
			
			line = reader2.readLine(); stFunkcij = Integer.parseInt(line);
			String t2[][] = new String[stFunkcij][], split[] = new String[2];
			for(i=0; i<stFunkcij; i++){
				line = reader2.readLine(); //opisFunkcije = Integer.parseInt(line);
				line = reader2.readLine(); stUkazov = Integer.parseInt(line);
				t2[i] = new String[stUkazov];
				for(j=0; j<stUkazov; j++){
					t2[i][j] = reader2.readLine();
					if(t2[i][j].charAt(1)=='U'){
						split = t2[i][j].split(" ");
						t2[i][j] = split[1];
					}
				}
			}
			metoda(0, t, t2);
			writer.write("["+v+","+s+"]\r\n"+u+"\r\n");
			
		}catch (FileNotFoundException ex){
		}catch (IOException ex){
        }catch (NumberFormatException ex){
		}catch (Exception ex){
		}finally {
			try {
				if(reader1!=null) reader1.close();
				if(reader2!=null) reader2.close();
				if(writer!=null) writer.close();
			}catch (IOException ex) {}
		}
	}

	public static void metoda(int i, char t[][], String t2[][]){
		for(int j=0; j<t2[i].length; j++){
			if(stK==0) return;
			if(t2[i][j].equals("FWD")){
				stK--;
				if(t[v+tp[u][0]][s+tp[u][1]]=='0'){ v+=tp[u][0]; s+=tp[u][1]; }
				else return;
			}
			else if(t2[i][j].equals("RGT")){ u=(u+1)%4; stK--; }
			else if(t2[i][j].equals("LFT")){ u=(u+3)%4; stK--; }
			else metoda(t2[i][j].charAt(0)-'1', t, t2);
		}
	}
	
	public static String vrniVpisnoStevilko(){
		return "63130344";
	}
}