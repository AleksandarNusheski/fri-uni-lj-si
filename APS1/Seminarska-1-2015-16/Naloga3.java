import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

class QueueElement{
	Object element;
	QueueElement next;

	QueueElement(){
		element = null;
		next = null;
	}
}
class Queue{
	//QueueElement -> QueueElement -> QueueElement -> ... -> QueueElement
	//    front                                                   rear
	//
	// nove elemente dodajamo za element 'rear'
	// elemente jemljemo s front strani
	private QueueElement front;
	private QueueElement rear;
	
	public Queue(){
		makenull();
	}
	
	public void makenull(){
		front = null;
		rear = null;
	}
	
	public boolean empty(){
		return (front == null);
	}
	
	public Object front(){
		if (!empty()) return front.element;
		else return null;
	}
	
	public void enqueue(Object obj){
		QueueElement el = new QueueElement();
		el.element = obj;
		el.next = null;
		
		if (empty()){front = el;}
		else{rear.next = el;}
		
		rear = el;
	}
	
	public void dequeue(){
		if (!empty()){
			front = front.next;
			
			if (front == null) rear = null;
		}
	}
}

class Pogodba{
	int x, y, sum; String poteze;
	Pogodba(int x, int y, String poteze, int sum){
		this.x = x; this.y = y; this.sum = sum+1;
		this.poteze = poteze+"["+(x+1)+", "+(y+1)+"]\r\n";
	}
}

public class Naloga3 {
	public static void obdelaj(String vhodnaDatoteka, String izhodnaDatoteka){
		BufferedReader reader = null; BufferedWriter writer = null;
		try{
			reader = new BufferedReader(new FileReader(vhodnaDatoteka));
			writer = new BufferedWriter(new FileWriter(izhodnaDatoteka));
			int n, m, i, j; String line;
			line = reader.readLine(); n = Integer.parseInt(line);
			line = reader.readLine(); m = Integer.parseInt(line);
			
			char t[][] = new char[n][m];
			for(i=n-1; i>=0; i--){
				line = reader.readLine();
				String[] split = line.split(",");
				for(j=0; j<m; j++)
					t[i][j]=split[j].charAt(0);
			}
			Queue queue = new Queue();
			boolean obstajaPot = false;
			for(i=0; i<m; i++)
				if(t[0][i]=='1' && t[1][i]=='1')
					queue.enqueue(new Pogodba(0, i, "", 0));
			
			Pogodba p;
			while (!queue.empty()){
				p = (Pogodba)queue.front(); queue.dequeue();
				if ((p.x < 0 || p.x >= t.length) || (p.y < 0 || p.y >= t[p.x].length)
					|| (t[p.x][p.y] == '0') || (t[p.x][p.y] == '.')) continue;
				if (t[p.x][p.y] == '1' && p.x==n-1){
					writer.write(p.sum+"\r\n"+p.poteze);
					obstajaPot = true; break;
				}
				t[p.x][p.y] = '.';
				queue.enqueue(new Pogodba(p.x, p.y-1, p.poteze, p.sum));
				queue.enqueue(new Pogodba(p.x, p.y+1, p.poteze, p.sum));
				queue.enqueue(new Pogodba(p.x+1, p.y, p.poteze, p.sum));
			}
			if (!obstajaPot) writer.write("0\r\n");
		}catch (FileNotFoundException ex){
		}catch (IOException ex){
		}catch (NumberFormatException ex){
		}catch (Exception ex){
		}finally {
			try {
				if(reader!=null) reader.close();
				if(writer!=null) writer.close();
			}catch (IOException ex) {}
		}
	}
	
	public static String vrniVpisnoStevilko(){
		return "63130344";
	}
}