## Description

My solutions of the tasks and homeworks at Faculty of Computer and Information Science, University of Ljubljana.

For every task there are instructions ("Navodila") on Slovenian language.

#### Content:
- Algorithms and data structures 1
- Algorithms and data structures 2